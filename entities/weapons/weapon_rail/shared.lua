if CLIENT then
    SWEP.PrintName = "Rail Gun"
    SWEP.Slot = 2
    SWEP.SlotPos = 3
    SWEP.DrawAmmo = false
    SWEP.DrawCrosshair = false
end

if SERVER then
	AddCSLuaFile("shared.lua")
end

SWEP.Author            = "Pivius"

SWEP.ViewModelFOV    = 65
SWEP.ViewModelFlip    = false

SWEP.Spawnable            = true
SWEP.AdminSpawnable        = true


SWEP.ViewModel      = "models/weapons/v_irifle.mdl"
SWEP.WorldModel   = "models/weapons/w_irifle.mdl"

-- Primary Fire Attributes --
SWEP.Primary.Recoil            = 0
SWEP.Primary.Damage            = 0
SWEP.Primary.NumShots        = 1
SWEP.Primary.Cone            = 0
SWEP.Primary.ClipSize        = 10
SWEP.Primary.DefaultClip    = 10
SWEP.Primary.Automatic       = false
SWEP.Primary.Ammo             = "ar2"

-- Secondary Fire Attributes --
SWEP.Secondary.Recoil        = 0
SWEP.Secondary.Damage        = 0
SWEP.Secondary.NumShots        = 1
SWEP.Secondary.Cone            = 0
SWEP.Secondary.ClipSize        = -1
SWEP.Secondary.DefaultClip    = -1
SWEP.Secondary.Automatic       = false
SWEP.Secondary.Ammo         = "none"

function SWEP:Initialize()
    self:SetWeaponHoldType("ar2")
    self.Wait = false
    self:SetDTBool( 1, false )
    self.Shootpos = Vector(0,0,0)
    self.HitPos = Vector(0,0,0)
    self.alpha = 255
    self.pos2 = Vector(0,0,0)
end

function SWEP:Precache()

end

function SWEP:Deploy()

    return true
end

function SWEP:Reload()

    return false
end

if SERVER then
  function SWEP:EquipAmmo(  ply )
    ply:GetActiveWeapon():SetClip1(ply:GetActiveWeapon():Clip1() + ply:GetAmmoCount( ply:GetActiveWeapon():GetPrimaryAmmoType()))
    ply:RemoveAmmo( ply:GetAmmoCount( ply:GetActiveWeapon():GetPrimaryAmmoType() ), ply:GetActiveWeapon():GetPrimaryAmmoType() )
  end
end
/*
function SWEP:Think()
  if self.Owner:GetAmmoCount(self.Weapon:GetPrimaryAmmoType()) > 0 then
    self.Weapon:SetClip1(self.Weapon:Clip1() + self.Owner:GetAmmoCount( self.Weapon:GetPrimaryAmmoType()))
    self.Owner:RemoveAmmo( self.Owner:GetAmmoCount( self.Weapon:GetPrimaryAmmoType() ), self.Weapon:GetPrimaryAmmoType() )
  end
end
*/
function SWEP:PrimaryAttack()
  if !IsFirstTimePredicted() then return end
	if self.Weapon:Clip1() == 0 then
		return
	end

  if self.Wait then return end
	local aimVec = self.Owner:GetAimVector()

  local tr = self.Owner:GetEyeTrace()
  local tracedata = {}
  tracedata.start = tr.StartPos- Vector(0, 0, 5) + aimVec * 26 + self.Owner:GetRight() * 4.5
  tracedata.endpos = tr.HitPos + (tr.Normal * 2)
  tracedata.filter = self.Owner
  tracedata.mask = MASK_PLAYERSOLID
  local trace = util.TraceLine(tracedata)
  if SERVER then
    if IsValid( trace.Entity ) then
      if trace.Entity != self:GetOwner() then
        local info = DamageInfo()
        info:SetDamage(80)
        info:SetAttacker(self.Owner)
        info:SetInflictor(self.Weapon)
        info:SetDamageForce(aimVec*1000)
        trace.Entity:TakeDamageInfo( info )
      end
  	end
    self.Shootpos = tracedata.start- Vector(0, 0, 5) + aimVec * 26 + self.Owner:GetRight() * 4.5
    self.HitPos = tracedata.endpos
  end

	self.Owner:MuzzleFlash()
  local effectdata = EffectData()

  		effectdata:SetOrigin( self:GetOwner():GetShootPos() )
  		effectdata:SetEntity( self.Weapon )
  		util.Effect( "effect_rail", effectdata, true )
	self.Weapon:EmitSound("weapons/airboat/airboat_gun_energy1.wav", 95, 100)
  local prevammo = self.Weapon:Clip1()
  self.Weapon:SetClip1(prevammo-1)
  self.Wait = true
  timer.Simple(1.5, function() self.Wait = false  end)
end
