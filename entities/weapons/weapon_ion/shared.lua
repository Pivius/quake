if CLIENT then
    SWEP.PrintName = "Ion Gun"
    SWEP.Slot = 2
    SWEP.SlotPos = 3
    SWEP.DrawAmmo = false
    SWEP.DrawCrosshair = false
end

if SERVER then
	AddCSLuaFile("shared.lua")
end

SWEP.Author            = "Pivius"

SWEP.ViewModelFOV    = 65
SWEP.ViewModelFlip    = false

SWEP.Spawnable            = true
SWEP.AdminSpawnable        = true


SWEP.ViewModel      = "models/weapons/v_crossbow.mdl"
SWEP.WorldModel   = "models/weapons/w_crossbow.mdl"

-- Primary Fire Attributes --
SWEP.Primary.Recoil            = 0
SWEP.Primary.Damage            = 0
SWEP.Primary.NumShots        = 1
SWEP.Primary.Cone            = 0
SWEP.Primary.ClipSize        = 10
SWEP.Primary.DefaultClip    = 100
SWEP.Primary.Automatic       = true
SWEP.Primary.Ammo             = "XBowBolt"

-- Secondary Fire Attributes --
SWEP.Secondary.Recoil        = 0
SWEP.Secondary.Damage        = 0
SWEP.Secondary.NumShots        = 1
SWEP.Secondary.Cone            = 0
SWEP.Secondary.ClipSize        = -1
SWEP.Secondary.DefaultClip    = -1
SWEP.Secondary.Automatic       = false
SWEP.Secondary.Ammo         = "none"

function SWEP:Initialize()
    self:SetWeaponHoldType("crossbow")
    self.Wait = false
    self.Wait2 = false
    self:SetDTBool( 0, false )
    self.Shootpos = Vector(0,0,0)
    self.HitPos = Vector(0,0,0)
    self.alpha = 255
    self.pos2 = self.Shootpos
    self:SetDTBool( 0, false )

end

function SWEP:Deploy()

    return true
end

function SWEP:Reload()
    return false
end

if SERVER then
function SWEP:EquipAmmo(  ply )
    ply:GetActiveWeapon():SetClip1(ply:GetActiveWeapon():Clip1() + ply:GetAmmoCount( ply:GetActiveWeapon():GetPrimaryAmmoType()))
    ply:RemoveAmmo( ply:GetAmmoCount( ply:GetActiveWeapon():GetPrimaryAmmoType() ), ply:GetActiveWeapon():GetPrimaryAmmoType() )
end
end


function SWEP:Tracer(pos, endpos, alpha)
  local Laser = Material( "trails/laser.vmt" )
  local col = Color(255, 155, 0, 255)

    	render.SetMaterial(Laser)
      render.DrawBeam(pos, endpos, 10, 0,0, Color(col.r, col.g, col.b, alpha))


end

function SWEP:ViewModelDrawn( )
  local aimVec = self.Owner:GetAimVector()
  local tr = self.Owner:GetEyeTrace()
  local tracedata = {}
  tracedata.start = tr.StartPos
  tracedata.endpos = tr.HitPos + (tr.Normal * 2)
  tracedata.filter = tr.Entity
  tracedata.mask = MASK_PLAYERSOLID
  local trace = util.TraceLine(tracedata)

  if !self:GetDTBool( 0 ) then
    self.alpha = self.alpha and Lerp(0.05, self.alpha, 1) or 1
    self.pos2 = self.pos2 and LerpVector(0.005, self.pos2, self.HitPos) or self.HitPos
  else
    self.alpha = self.alpha and Lerp(0.1, self.alpha, 255) or 255
    self.HitPos = tracedata.endpos
    self.Shootpos =self:GetOwner():GetAttachment(self:GetOwner():LookupAttachment( "anim_attachment_RH" )).Pos
    self.pos2 = self.Shootpos
  end
  cam.Start3D(EyePos(), EyeAngles())
  self:Tracer(self.pos2+Vector(0,0,15) + (aimVec * 26), self.HitPos, self.alpha)
  cam.End3D()
end

if SERVER then
  function SWEP:PrimaryAttack()
    if !self.Owner:IsValid() then return end
  	if self.Weapon:Clip1() == 0 then
  		return
  	end
    local rf = RecipientFilter()
    rf:AddAllPlayers()
    rf:RemovePlayer(self:GetOwner())

    self.Owner:MuzzleFlash()
    local effectdata = EffectData()
    effectdata:SetOrigin( self:GetOwner():GetAttachment(self:GetOwner():LookupAttachment( "anim_attachment_RH" )).Pos )
    effectdata:SetEntity( self.Weapon )
    util.Effect( "effect_ion", effectdata, true, rf )
  	local aimVec = self.Owner:GetAimVector()
    local tr = self.Owner:GetEyeTrace()
    local tracedata = {}
    tracedata.start = tr.StartPos- Vector(0, 0, 5) + aimVec * 26 + self.Owner:GetRight() * 4.5
    tracedata.endpos = tr.HitPos + (tr.Normal * 2)
    tracedata.filter = self.Owner
    tracedata.mask = MASK_PLAYERSOLID
    local trace = util.TraceLine(tracedata)
    if self.Wait then return end
      if IsValid( trace.Entity ) then
        if trace.Entity != self:GetOwner() then
          local info = DamageInfo()
          info:SetDamage(6)
          info:SetAttacker(self.Owner)
          info:SetInflictor(self.Weapon)
          info:SetDamageForce(aimVec*1000)
          trace.Entity:TakeDamageInfo( info )
        end
      end


  	self.Weapon:EmitSound("weapons/airboat/airboat_gun_energy1.wav", 95, 100)
    local prevammo = self.Weapon:Clip1()
    self.Weapon:SetClip1(prevammo-1)
    self.Wait = true
    timer.Simple(0.044, function() self.Wait = false  end)
  end


else
  function SWEP:PrimaryAttack()
    if !self.Owner:IsValid() then return end
  	if self.Weapon:Clip1() == 0 then
  		return
  	end
    self.Owner:MuzzleFlash()
  	local aimVec = self.Owner:GetAimVector()
    local tr = self.Owner:GetEyeTrace()
    local tracedata = {}
    tracedata.start = tr.StartPos- Vector(0, 0, 5) + aimVec * 26 + self.Owner:GetRight() * 4.5
    tracedata.endpos = tr.HitPos + (tr.Normal * 2)
    tracedata.filter = self.Owner
    tracedata.mask = MASK_PLAYERSOLID
    local trace = util.TraceLine(tracedata)
    self:SetDTBool( 0, true )
    timer.Simple(0.1, function()  self:SetDTBool( 0, false ) end)
    if self.Wait2 then return end
      self.Wait2 = true
      self.Weapon:EmitSound("weapons/airboat/airboat_gun_energy1.wav", 95, 100)
      timer.Simple(0.044, function() self.Wait2 = false  self.Weapon:EmitSound("weapons/airboat/airboat_gun_energy1.wav", 95, 100) end)
  end

end
