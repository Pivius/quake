if CLIENT then
    SWEP.PrintName = "Ion Gun"
    SWEP.Slot = 2
    SWEP.SlotPos = 3
    SWEP.DrawAmmo = false
    SWEP.DrawCrosshair = false
end

if SERVER then
	AddCSLuaFile("shared.lua")
end

SWEP.Author            = "Pivius"

SWEP.ViewModelFOV    = 65
SWEP.ViewModelFlip    = false

SWEP.Spawnable            = true
SWEP.AdminSpawnable        = true


SWEP.ViewModel      = "models/weapons/v_crossbow.mdl"
SWEP.WorldModel   = "models/weapons/w_crossbow.mdl"

-- Primary Fire Attributes --
SWEP.Primary.Recoil            = 0
SWEP.Primary.Damage            = 0
SWEP.Primary.NumShots        = 1
SWEP.Primary.Cone            = 0
SWEP.Primary.ClipSize        = 10
SWEP.Primary.DefaultClip    = 100
SWEP.Primary.Automatic       = true
SWEP.Primary.Ammo             = "XBowBolt"

-- Secondary Fire Attributes --
SWEP.Secondary.Recoil        = 0
SWEP.Secondary.Damage        = 0
SWEP.Secondary.NumShots        = 1
SWEP.Secondary.Cone            = 0
SWEP.Secondary.ClipSize        = -1
SWEP.Secondary.DefaultClip    = -1
SWEP.Secondary.Automatic       = false
SWEP.Secondary.Ammo         = "none"

function SWEP:Initialize()
    self:SetWeaponHoldType("crossbow")
    self.Wait = false
    self:SetDTBool( 0, false )


end

function SWEP:Precache()

end

function SWEP:Deploy()

    return true
end

function SWEP:Reload()
    return false
end
if SERVER then
function SWEP:EquipAmmo(  ply )
    ply:GetActiveWeapon():SetClip1(ply:GetActiveWeapon():Clip1() + ply:GetAmmoCount( ply:GetActiveWeapon():GetPrimaryAmmoType()))
    ply:RemoveAmmo( ply:GetAmmoCount( ply:GetActiveWeapon():GetPrimaryAmmoType() ), ply:GetActiveWeapon():GetPrimaryAmmoType() )
end
end



function SWEP:Tracer(pos, endpos)
  local Laser = Material( "trails/laser.vmt" )
  local col = Color(255, 200, 0, 255)
  local matalpha = 255
  matalpha = 1
  alpha = 255
  pos2 = pos
      alpha = alpha and Lerp(0.02, alpha, matalpha) or matalpha
      pos2 = pos2 and LerpVector(0.005, pos2, endpos) or endpos
    	render.SetMaterial(Laser)
    	render.DrawBeam(pos2, endpos, 10, 0,0, Color(col.r, col.g, col.b, math.Clamp(alpha, matalpha, col.a)))
end

function SWEP:DrawWorldModel( )
  self:DrawModel( )

  if not self.Owner:IsValid( ) then
    return
  end
  if !self:GetDTBool( 0 ) then return end
    local aimVec = self.Owner:GetAimVector()
    local tr = self.Owner:GetEyeTrace()
    local tracedata = {}
    tracedata.start = tr.StartPos
    tracedata.endpos = tr.HitPos + (tr.Normal * 2)
    tracedata.filter = tr.Entity
    tracedata.mask = MASK_PLAYERSOLID
    local trace = util.TraceLine(tracedata)

    self:Tracer(tracedata.start - Vector(0,0,25) + (aimVec * 26) + self.Owner:GetRight() * 12.5, tracedata.endpos)
end

function SWEP:ViewModelDrawn( )
  if !self:GetDTBool( 0 ) then return end
    local aimVec = self.Owner:GetAimVector()
    local tr = self.Owner:GetEyeTrace()
    local tracedata = {}
    tracedata.start = tr.StartPos
    tracedata.endpos = tr.HitPos + (tr.Normal * 2)
    tracedata.filter = tr.Entity
    tracedata.mask = MASK_PLAYERSOLID
    local trace = util.TraceLine(tracedata)
    self:Tracer(tracedata.start - Vector(0,0,25) + (aimVec * 26) + self.Owner:GetRight() * 12.5, tracedata.endpos)
end

function SWEP:PrimaryAttack()

	if self.Weapon:Clip1() == 0 then
		return
	end

	local aimVec = self.Owner:GetAimVector()
  local tr = self.Owner:GetEyeTrace()
  local tracedata = {}
  tracedata.start = tr.StartPos
  tracedata.endpos = tr.HitPos + (tr.Normal * 2)
  tracedata.filter = tr.Entity
  tracedata.mask = MASK_PLAYERSOLID
  local trace = util.TraceLine(tracedata)
  if SERVER then
    if IsValid( trace.Entity ) then
      if trace.Entity != self.Owner then
        trace.Entity:TakeDamage( 5, tracedata.filter, self.Weapon )
      end
    end
  end

  self:SetDTBool( 0, true )
  if self.Wait then return end

	self.Weapon:EmitSound("weapons/airboat/airboat_gun_energy1.wav", 95, 100)
  local prevammo = self.Weapon:Clip1()
  self.Weapon:SetClip1(prevammo-1)
  self.Wait = true
  timer.Simple(0.1, function() self.Wait = false self:SetDTBool( 0, false ) end)
end
