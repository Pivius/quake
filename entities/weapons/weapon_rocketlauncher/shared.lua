if CLIENT then
    SWEP.PrintName = "Rocket Launcher"
    SWEP.Slot = 3
    SWEP.SlotPos = 3
    SWEP.DrawAmmo = false
    SWEP.DrawCrosshair = false
end

if SERVER then
	AddCSLuaFile("shared.lua")
end

SWEP.Author            = "Pivius"

SWEP.ViewModelFOV    = 65
SWEP.ViewModelFlip    = false

SWEP.Spawnable            = true
SWEP.AdminSpawnable        = true


SWEP.ViewModel      = "models/weapons/v_rpg.mdl"
SWEP.WorldModel   = "models/weapons/w_rocket_launcher.mdl"

-- Primary Fire Attributes --
SWEP.Primary.Recoil            = 0
SWEP.Primary.Damage            = 0
SWEP.Primary.NumShots        = 1
SWEP.Primary.Cone            = 0
SWEP.Primary.ClipSize        = 10
SWEP.Primary.DefaultClip    = 10
SWEP.Primary.Automatic       = false
SWEP.Primary.Ammo             = "RPG_Round"

-- Secondary Fire Attributes --
SWEP.Secondary.Recoil        = 0
SWEP.Secondary.Damage        = 0
SWEP.Secondary.NumShots        = 1
SWEP.Secondary.Cone            = 0
SWEP.Secondary.ClipSize        = -1
SWEP.Secondary.DefaultClip    = -1
SWEP.Secondary.Automatic       = false
SWEP.Secondary.Ammo         = "none"

function SWEP:Initialize()
    self:SetWeaponHoldType("rpg")
    self.Wait = false
end

function SWEP:Precache()

end

function SWEP:Deploy()

    return true
end

function SWEP:Reload()

    return false
end

if SERVER then
  function SWEP:EquipAmmo(  ply )
    ply:GetActiveWeapon():SetClip1(ply:GetActiveWeapon():Clip1() + ply:GetAmmoCount( ply:GetActiveWeapon():GetPrimaryAmmoType()))
    ply:RemoveAmmo( ply:GetAmmoCount( ply:GetActiveWeapon():GetPrimaryAmmoType() ), ply:GetActiveWeapon():GetPrimaryAmmoType() )
  end
end
/*
function SWEP:Think()
  if self.Owner:GetAmmoCount(self.Weapon:GetPrimaryAmmoType()) > 0 then
    self.Weapon:SetClip1(self.Weapon:Clip1() + self.Owner:GetAmmoCount( self.Weapon:GetPrimaryAmmoType()))
    self.Owner:RemoveAmmo( self.Owner:GetAmmoCount( self.Weapon:GetPrimaryAmmoType() ), self.Weapon:GetPrimaryAmmoType() )
  end
end*/

function SWEP:PrimaryAttack()

  if self.Wait then return end
	if self.Weapon:Clip1() == 0 then
		return
	end

	local aimVec = self.Owner:GetAimVector()

	if SERVER then
		local missile = ents.Create("send_rpg_missile")
		local offset = Vector(aimVec.x, aimVec.y , 0) + Vector(0, 0, 5) - self.Owner:GetRight() * 2.5
		missile:SetPos(self.Owner:GetShootPos() - Vector(0, 0, 5) + self.Owner:GetRight() * 2.5)
		missile:SetAngles(self.Owner:EyeAngles() )
    missile.Weapon = self
		missile:Spawn()
		missile:SetOwner(self.Owner)
		missile:SetVelocity(aimVec * 1000 + offset)


		self.Weapon.Missile = missile

	end

	self.Weapon:EmitSound("weapons/rpg/rocketfire1.wav", 95, 100)
	--self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
	--self.Owner:GetViewModel():SetPlaybackRate(3)
  local prevammo = self.Weapon:Clip1()
  self.Weapon:SetClip1(prevammo-1)
  self.Wait = true
  timer.Simple(0.7, function() self.Wait = false end)
end
