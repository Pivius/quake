  if CLIENT then
    SWEP.PrintName = "Shotgun"
    SWEP.Slot = 1
    SWEP.SlotPos = 3
    SWEP.DrawAmmo = false
    SWEP.DrawCrosshair = false
end

if SERVER then
	AddCSLuaFile("shared.lua")
end

SWEP.Author            = "Pivius"

SWEP.ViewModelFOV    = 65
SWEP.ViewModelFlip    = false

SWEP.Spawnable            = true
SWEP.AdminSpawnable        = true


SWEP.ViewModel      = "models/weapons/v_shotgun.mdl"
SWEP.WorldModel   = "models/weapons/w_shotgun.mdl"

-- Primary Fire Attributes --
SWEP.Primary.Recoil            = 0
SWEP.Primary.Damage            = 0
SWEP.Primary.NumShots        = 1
SWEP.Primary.Cone            = 0
SWEP.Primary.ClipSize        = 10
SWEP.Primary.DefaultClip    = 10
SWEP.Primary.Automatic       = false
SWEP.Primary.Ammo             = "Buckshot"

-- Secondary Fire Attributes --
SWEP.Secondary.Recoil        = 0
SWEP.Secondary.Damage        = 0
SWEP.Secondary.NumShots        = 1
SWEP.Secondary.Cone            = 0
SWEP.Secondary.ClipSize        = -1
SWEP.Secondary.DefaultClip    = -1
SWEP.Secondary.Automatic       = false
SWEP.Secondary.Ammo         = "none"

function SWEP:Initialize()
    self:SetWeaponHoldType("shotgun")
    self.Wait = false
    self.Shootpos = Vector(0,0,0)
    self.HitPos = Vector(0,0,0)
    self.alpha = 255
    self.pos2 = Vector(0,0,0)
end

function SWEP:Precache()

end

function SWEP:Deploy()

    return true
end

function SWEP:Reload()

    return false
end



if SERVER then
  function SWEP:EquipAmmo(  ply )
    ply:GetActiveWeapon():SetClip1(ply:GetActiveWeapon():Clip1() + ply:GetAmmoCount( ply:GetActiveWeapon():GetPrimaryAmmoType()))
    ply:RemoveAmmo( ply:GetAmmoCount( ply:GetActiveWeapon():GetPrimaryAmmoType() ), ply:GetActiveWeapon():GetPrimaryAmmoType() )
  end
end

function SWEP:CreateBullet(num, src, dir, spread, tracer, force, damage)
  local aimVec = self.Owner:GetAimVector()
  local bullet = {}
	bullet.Num 		= num
	bullet.Src 		= src
	bullet.Dir 		= dir
	bullet.Spread 	= spread
	bullet.Tracer	= tracer
	bullet.Force	= force
	bullet.Damage	= 0

	bullet.Callback = function(ent, tr, info)
    if tr.HitPos then
      local tracedata = {}
      tracedata.start = tr.StartPos
      tracedata.endpos = tr.HitPos + (tr.Normal * 2)
      tracedata.filter = ent
      tracedata.mask = MASK_PLAYERSOLID
      local trace = util.TraceLine(tracedata)
      if SERVER then
        if IsValid( trace.Entity ) then
          if trace.Entity != self:GetOwner() then
            local info = DamageInfo()
            info:SetDamage(damage)
            info:SetAttacker(self.Owner)
            info:SetInflictor(self.Weapon)
            info:SetDamageForce(aimVec*1000)
            trace.Entity:TakeDamageInfo( info )
          end
        end
      end
    end
	end
  self.Owner:FireBullets( bullet )
end

function SWEP:distanceFrom(A,B)
   return math.sqrt((B.x - A.x) ^ 2 + (B.y - A.y) ^ 2 + (B.z - A.z) ^ 2)
end



function SWEP:PrimaryAttack()
	if self.Weapon:Clip1() == 0 then
		return
	end

  if self.Wait then return end
	local aimVec = self.Owner:GetAimVector()
  local tr = self.Owner:GetEyeTrace()
  local tracedata = {}
  tracedata.start = tr.StartPos
  tracedata.endpos = tr.HitPos + (tr.Normal * 2)
  tracedata.mask = MASK_PLAYERSOLID
  local trace = util.TraceLine(tracedata)
  local dist = (self:distanceFrom(tracedata.start, tracedata.endpos)/15)/800
  local aimAng = aimVec:Angle()
  for i=0, 360, 360/7 do
    local p1,p2,p3
    local rad = i * (math.pi/180)
    p1 = math.cos(rad +(math.pi/180)+ 180)
    p2 = math.sin(rad +(math.pi/180)+ 180)

    p3 = math.sin(rad+(math.pi/180)+ 180)
    aim = aimAng+ Angle(p1,p2,0)
    aim2 = aimAng:Up():Angle() --+Angle(math.cos(rad +(math.pi/180)+ rot)*dist,p3,math.sin(rad +(math.pi/180)+ rot)*dist)
    local newvec = aimAng:Forward() +
    p1*aimAng:Right()*dist +
    p2*aimAng:Up()*dist

    self:CreateBullet(1, self.Owner:GetShootPos(), newvec, Vector( 0, 0, 0 ), 1, 5, 5)
end

for i=0, 360, 360/7 do
  local p1,p2,p3
  local rad = i * (math.pi/180)
  p1 = math.cos(rad +(math.pi/180)+ 180)
  p2 = math.sin(rad +(math.pi/180)+ 180)

  p3 = math.sin(rad+(math.pi/180)+ 180)
  aim = aimAng+ Angle(p1,p2,0)
  aim2 = aimAng:Up():Angle() --+Angle(math.cos(rad +(math.pi/180)+ rot)*dist,p3,math.sin(rad +(math.pi/180)+ rot)*dist)
  local newvec = aimAng:Forward() +
  p1*aimAng:Right()*dist/2 +
  p2*aimAng:Up()*dist/2

  self:CreateBullet(1, self.Owner:GetShootPos(), newvec, Vector( 0, 0, 0 ), 1, 5, 5)
end

	self.Owner:MuzzleFlash()
	self.Weapon:EmitSound("weapons/shotgun/shotgun_fire6.wav", 95, 100)
  local prevammo = self.Weapon:Clip1()
  self.Weapon:SetClip1(prevammo-1)
  self.Wait = true
  timer.Simple(1, function() self.Wait = false  end)
end
