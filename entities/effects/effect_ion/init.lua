function EFFECT:Init( effectdata )
	self.Weapon = effectdata:GetEntity()
	if !effectdata:GetEntity():IsValid() then return false end

	self.Forw = self.Weapon:GetForward()

	self.Alpha = 255--0
	local owner = self.Weapon:GetOwner()
	local aimVec = owner:GetAimVector()
	local tr = owner:GetEyeTrace()
	local tracedata = {}
	tracedata.start = tr.StartPos
	tracedata.endpos = tr.HitPos + (tr.Normal * 2)
	tracedata.filter = tr.Entity
	tracedata.mask = MASK_PLAYERSOLID

	self.endpos = tracedata.endpos
	self.Pos2 = effectdata:GetOrigin()+ aimVec * 26
	--self.Pos = effectdata:GetOrigin()
	self.Entity:SetPos( effectdata:GetOrigin() )
	self:SetRenderBoundsWS( effectdata:GetOrigin(), self.endpos )

end

function EFFECT:Think()

	if !self.Weapon:IsValid() then return false end


	self.Alpha = self.Alpha and Lerp(0.4, self.Alpha, 150) or 150
	--self.Pos2 = self.Pos2 and LerpVector(0.01, self.Pos2, self.endpos) or self.endpos

	return math.Round(self.Alpha) != 150

end

function EFFECT:Render()
	if !self.Weapon:IsValid() then return false end
	if math.Round(self.Alpha) == 150 then return end

	local Laser = Material( "trails/laser.vmt" )
  local col = Color(255, 155, 0, 255)
  render.SetMaterial(Laser)
  render.DrawBeam(self.Pos2, self.endpos, 10, 0,0, Color(col.r, col.g, col.b, math.Clamp(self.Alpha, 1, col.a)))

end
