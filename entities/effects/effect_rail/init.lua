function EFFECT:Init( effectdata )

	self.Attachment = "1"--effectdata:GetAttachment()

	self.Weapon = effectdata:GetEntity()

	self.Forw = self.Weapon:GetForward()

	self.Alpha = 255--0

	local owner = self.Weapon:GetOwner()
	local aimVec = owner:GetAimVector()
	local tr = owner:GetEyeTrace()
	local tracedata = {}
	tracedata.start = tr.StartPos
	tracedata.endpos = tr.HitPos + (tr.Normal * 2)
	tracedata.filter = tr.Entity
	tracedata.mask = MASK_PLAYERSOLID

	self.endpos = tracedata.endpos
	self.Pos2 = tracedata.start- Vector(0, 0, 5) + aimVec * 26 + owner:GetRight() * 4.5
	self.Pos = effectdata:GetOrigin()
	self.Entity:SetPos( self.Pos )


end

function EFFECT:Think()

	if !self.Weapon:IsValid() then return false end

	self:SetRenderBoundsWS( self.Pos, self.endpos )
	self.Alpha = self.Alpha and Lerp(0.02, self.Alpha, 1) or 1
	self.Pos2 = self.Pos2 and LerpVector(0.001, self.Pos2, self.endpos) or self.endpos

	return math.Round(self.Alpha) != 1

end

function EFFECT:Render()
	if math.Round(self.Alpha) == 1 then return end

	local Laser = Material( "trails/electric.vmt" )
  local col = Color(155, 255, 255, 255)
  render.SetMaterial(Laser)
  render.DrawBeam(self.Pos2, self.endpos, 10, math.random(50,60),math.random(90,100), Color(col.r, col.g, col.b, math.Clamp(self.Alpha, 1, col.a)))

end
