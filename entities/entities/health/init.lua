AddCSLuaFile('cl_init.lua')
AddCSLuaFile('shared.lua')

include('shared.lua')
include('quake/gamemode/sv_health.lua')
local activated = false
function ENT:Initialize()


  self:SetModel("models/Health_model/health_model.mdl")
  local min, max = self:GetModelBounds()
  self.min = min + self:GetPos()
  self.max = max + self:GetPos()
  local BBOX = (self.max - self.min)
	self:SetSolid( SOLID_BBOX )
	self:PhysicsInitBox( -BBOX, BBOX )
	self:SetCollisionBoundsWS( self.min, self.max )

	self:SetTrigger( true )
	self:DrawShadow( false )
	self:SetNotSolid( true )
	self:SetNoDraw( false )
  self:SetType( self.Type )
  self:SetOrigin( self:GetPos() )
  self:SetDisplay( true )
  self:SetCooldown( self.Cooldown )

/*
	self.Phys = self:GetPhysicsObject()
	if self.Phys and self.Phys:IsValid() then
		self.Phys:Sleep()
		self.Phys:EnableCollisions( false )
	end*/
end

function ENT:Think()
  local ang = self:GetAngles()
  ang:RotateAroundAxis(self:GetUp() , FrameTime()*64)
  self:SetAngles(ang)

  for k,v in pairs (player.GetAll()) do
    if !v:GetMega() then
      if !timer.Exists("Mega" .. self:EntIndex()) then
        timer.Create("Mega" .. self:EntIndex(), self:GetCooldown(), 1, function() self:SetDisplay( true ) timer.Remove("Mega" .. self:EntIndex()) end)
      end
    else
      timer.Remove("Mega" .. self:EntIndex())
    end
  end
end

function ENT:StartTouch(ent)
    if ent:IsPlayer() then
      if self:GetDisplay() then
        local HP = ent:Health()
        print(ent:GetMega())
        if self:GetType() == "Green" then
          if HP != 200 then
            if (HP+ 5) > 200 then
              ent:SetHealth(200)
              self:SetDisplay( false )
              timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
            else
              ent:SetHealth(HP + 5)
              self:SetDisplay( false )
              timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
            end
          end
        elseif self:GetType() == "Yellow" then
          if (HP+ 25) > 100 and HP != 100 and !ent:GetMega() and not(HP > 100) then
            self:SetDisplay( false )
            timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
            ent:SetHealth(100)
          elseif (HP) < 100 and !ent:GetMega() then
            self:SetDisplay( false )
            timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
            ent:SetHealth(HP + 25)
          elseif (HP) > 100 and !ent:GetMega() then
            if (HP+ 25) > 200 then
              self:SetDisplay( false )
              ent:SetHealth(200)
              timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
            else
              self:SetDisplay( false )
              ent:SetHealth(HP + 25)
              timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
            end
          elseif ent:GetMega() and HP != 200 then
            if (HP+ 25) > 200 then
              ent:SetHealth(200)
              self:SetDisplay( false )
              timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
            else
              ent:SetHealth(HP + 25)
              self:SetDisplay( false )
              timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
            end
          end
        elseif self:GetType() == "Orange" then
          if (HP+ 50) > 100 and HP != 100 and !ent:GetMega() and not(HP > 100) then
            ent:SetHealth(100)
            self:SetDisplay( false )
            timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
          elseif (HP) < 100 and !ent:GetMega() then
            ent:SetHealth(HP + 50)
            self:SetDisplay( false )
            timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
          elseif (HP) > 100 and !ent:GetMega() then
            if (HP+ 50) > 200 then
              self:SetDisplay( false )
              ent:SetHealth(200)
              timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
            else
              self:SetDisplay( false )
              ent:SetHealth(HP + 50)
              timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
            end
          elseif ent:GetMega() and HP != 200 then
            if (HP+ 50) > 200 then
              ent:SetHealth(200)
              self:SetDisplay( false )
              timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
            else
              ent:SetHealth(HP + 50)
              self:SetDisplay( false )
              timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
            end
          end
        elseif self:GetType() == "Blue" then
          if (HP+ 100) > 200 and HP != 200 then
            ent:SetMega(true)
            self:SetDisplay( false )
            ent:SetHealth(200)
          elseif (HP + 100) <= 200 then
            ent:SetMega(true)
            self:SetDisplay( false )
            ent:SetHealth(HP + 100)
          end
        end
      end
    end
end
