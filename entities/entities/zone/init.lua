AddCSLuaFile('cl_init.lua')
AddCSLuaFile('shared.lua')

include('trick/gamemode/sv_tricks.lua')
include('shared.lua')
local Surfbool = false
local Groundbool = false
function ENT:Initialize()
	local BBOX = (self.max - self.min)
	self:SetSolid( SOLID_BBOX )
	self:PhysicsInitBox( -BBOX, BBOX )
	self:SetCollisionBoundsWS( self.min, self.max )

	self:SetTrigger( true )
	self:DrawShadow( false )
	self:SetNotSolid( true )
	self:SetNoDraw( false )

	self.Phys = self:GetPhysicsObject()
	if self.Phys and self.Phys:IsValid() then
		self.Phys:Sleep()
		self.Phys:EnableCollisions( false )
	end
	self:SetZoneName( self.name )
	self:SetSurfing( self.surfing )
end


function ENT:StartTouch( ent )
	if IsValid( ent ) and ent:IsPlayer() and ent:Team() != TEAM_SPECTATOR then
		local zone = self:GetZoneName()

		if self:GetSurfing() == 0 then
			if ent:GetZoneOrder() == nil or table.GetLastValue(ent:GetZoneOrder()) != self.name then
				PrintMessage( HUD_PRINTTALK, ent:Nick() .. " has entered " .. zone )
				print(self:GetSurfing())
				ent:ZoneOrder( zone, "entered" )
				ent:TrackTricks()
			end
		end
	end
end

function Surfing( ent )
        local vPos = ent:GetPos()

        local vMins = ent:OBBMins()

        local vMaxs = ent:OBBMaxs()

        local vEndPos = vPos * 1
        vEndPos.z = vEndPos.z -1

        local tr = util.TraceHull{
            start = vPos,
            endpos = vEndPos,
            mins = vMins,
            maxs = vMaxs,
            mask = MASK_PLAYERSOLID_BRUSHONLY,

            filter = function(e1, e2)
                return not e1:IsPlayer()
            end

        }
        if(tr.Fraction ~= 1) then
            -- Gets the normal vector of the surface under the player
            local vPlane, vLast = tr.HitNormal, Vector()

            -- Make sure it's not flat ground and not a surf ramp (1.0 = flat ground, < 0.7 = surf ramp)
            if(0.2 <= vPlane.z and vPlane.z < 1) then
							return true
						else

							return false
            end
					else
						return false
        end
end
function ENT:Touch( ent )
	if IsValid( ent ) and ent:IsPlayer() and ent:Team() != TEAM_SPECTATOR then
		local zone = self:GetZoneName()
		local surfing = Surfing( ent )

		if self:GetSurfing() == 1 then
			if ent:GetZoneOrder() == nil or table.GetLastValue(ent:GetZoneOrder()) != self.name then
				if surfing == true and !ent:OnGround() and Surfbool == false then
					Surfbool = true
					--PrintMessage( HUD_PRINTTALK, ent:Nick() .. " is Surfing on " .. zone )
					ent:ZoneOrder( zone, "surfing" )
					ent:TrackTricks()
				end
			elseif surfing == false and !ent:OnGround() and Surfbool == true then
					Surfbool = false
					--ent:ZoneOrder( zone, "not surfing" )
					--ent:TrackTricks()
					--PrintMessage( HUD_PRINTTALK, ent:Nick() .. " stopped Surfing on " .. zone )
			end
		elseif self:GetSurfing() == 2 then
			if ent:GetZoneOrder() == nil or table.GetLastValue(ent:GetZoneOrder()) != self.name then
				if ent:OnGround() and Groundbool == false then
					Groundbool = true
					PrintMessage( HUD_PRINTTALK, ent:Nick() .. " landed on " .. zone )
					ent:ZoneOrder( zone, "ground" )
					ent:TrackTricks()
				end
			elseif !ent:OnGround() and Groundbool == true then
				PrintMessage( HUD_PRINTTALK, ent:Nick() .. " has left " .. zone )
					Groundbool = false
			end
		end
	end
end

function ENT:EndTouch( ent )
	if IsValid( ent ) and ent:IsPlayer() and ent:Team() != TEAM_SPECTATOR then
		local zone = self:GetZoneName()
		if self:GetSurfing() == 0 then
			PrintMessage( HUD_PRINTTALK, ent:Nick() .. " has left " .. zone )
			--ent:ZoneOrder( zone, "left" )
			--ent:TrackTricks()
		elseif self:GetSurfing() == 1 and Surfbool == true then
			Surfbool = false
			--ent:ZoneOrder( zone, "not surfing" )
			--ent:TrackTricks()
			--PrintMessage( HUD_PRINTTALK, ent:Nick() .. " stopped Surfing on " .. zone )
		elseif self:GetSurfing() == 2 and Groundbool == true then
			PrintMessage( HUD_PRINTTALK, ent:Nick() .. " has left " .. zone )
			Groundbool = false
		end
	end
end
