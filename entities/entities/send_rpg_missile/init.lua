AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

function ENT:Initialize()

	self.Entity:SetModel("models/weapons/W_missile_closed.mdl")
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetMoveType(MOVETYPE_FLY)
	self.Entity:SetSolid(SOLID_VPHYSICS)
	--self.Entity:SetCollisionGroup(COLLISION_GROUP_NPC)
	self.Entity:SetGravity(0)
	local phys = self.Entity:GetPhysicsObject()
	if SERVER then
		self.Entity.aim = Vector(0,0,0)
	end
	if phys and phys:IsValid() then
		phys:Wake()
	end

	self.Entity.RocketSound = CreateSound(self.Entity, "weapons/rpg/rocket1.wav")
	self.Entity.RocketSound:SetSoundLevel(95)
	self.Entity.RocketSound:Play()
	self.Direct = false
end

function ENT:OnTakeDamage(dmginfo)
	return false
end

function ENT:Use(activator, caller)
	return false
end

function ENT:StartTouch(ent)
	local owner = self.Entity:GetOwner()
	if ent != owner and ent:GetClass() != "trigger_soundscape" then
		if ent:IsPlayer() then
			self.Target = ent
			self.Direct = true
		end
		self.Entity:Explode()
	end
end


function ENT:OnRemove()
	self.Entity.RocketSound:Stop()
end

function ENT:Explode()
	/*
	local InSphere = ents.FindInSphere( self.Entity:GetPos(), 150 )
	for k,v in ipairs(InSphere) do
		if(v:IsPlayer()) then
			if(v == self.Entity:GetOwner()) then
				v:SetVelocity(v:GetAimVector() * -500, 0)
			end
		end
	end
*/
	if self.Entity.BlewUp then
		return
	end

	local owner = self.Entity:GetOwner()

	local ED = EffectData()
	ED:SetOrigin(self.Entity:GetPos())

	util.Effect("Explosion", ED)
	local dmginfo = DamageInfo()
	dmginfo:SetInflictor(self.Weapon)
	dmginfo:SetAttacker(owner)
	dmginfo:SetDamage(100)
	dmginfo:SetDamageType( 2 )
	dmginfo:SetDamagePosition(self:GetPos())
	if self.Direct then

		--self.Target:TakeDamageInfo( dmginfo )
	end
	util.BlastDamageInfo( dmginfo, self.Entity:GetPos(), 175 )
	SafeRemoveEntity(self.Entity)
end
