AddCSLuaFile('cl_init.lua')
AddCSLuaFile('shared.lua')

include('shared.lua')
local activated = false
function ENT:Initialize()


  self:SetModel( self.Model )
  local min, max = self:GetModelBounds()
  self.min = min + self:GetPos()
  self.max = max + self:GetPos()
  local BBOX = (self.max - self.min)
	self:SetSolid( SOLID_BBOX )
	self:PhysicsInitBox( -BBOX, BBOX )
	self:SetCollisionBoundsWS( self.min, self.max )

	self:SetTrigger( true )
	self:DrawShadow( false )
	self:SetNotSolid( true )
	self:SetNoDraw( false )
  self:SetWeapon( self.Weapon )
  self:SetOrigin( self:GetPos() )
  self:SetDisplay( true )
  self:SetCooldown( self.Cooldown )

  self:SetRed(self.color.r)
  self:SetGreen(self.color.g)
  self:SetBlue(self.color.b)
/*
	self.Phys = self:GetPhysicsObject()
	if self.Phys and self.Phys:IsValid() then
		self.Phys:Sleep()
		self.Phys:EnableCollisions( false )
	end*/
end

function ENT:Think()
  local ang = self:GetAngles()
  ang:RotateAroundAxis(self:GetUp() , FrameTime()*64)
  self:SetAngles(ang)



end

function ENT:StartTouch(ent)
    if ent:IsPlayer() then
      if self:GetDisplay() then
        self:SetDisplay( false )

        if ent:HasWeapon(self:GetWeapon()) then
          for k,v in pairs (ent:GetWeapons()) do
            if v:GetClass() == self:GetWeapon() then
              ent:GiveAmmo( 10, game.GetAmmoName(v:GetPrimaryAmmoType()), false )
            end
          end
        else
          ent:Give(self:GetWeapon())
        end
        timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
      end
    end
end
