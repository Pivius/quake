include('shared.lua')

function ENT:Draw()
--self:DrawModel()
  if self:GetDisplay() then
    local pos = LocalPlayer():EyePos()+LocalPlayer():EyeAngles():Forward()*10
    local ang = LocalPlayer():EyeAngles()
    ang = Angle(ang.p+90,ang.y,0)
    render.ClearStencil()
    render.SetStencilEnable(true)
      render.SetStencilWriteMask(255)
      render.SetStencilTestMask(255)
      render.SetStencilReferenceValue(15)
      render.SetStencilFailOperation(STENCILOPERATION_KEEP)
      render.SetStencilZFailOperation(STENCILOPERATION_KEEP)
      render.SetStencilPassOperation(STENCILOPERATION_REPLACE)
      render.SetStencilCompareFunction(STENCILCOMPARISONFUNCTION_ALWAYS)
      render.SetBlend(0) --don't visually draw, just stencil
      scale = Vector(1,1,1)
      mat = Matrix()
      mat:Scale(Vector(scale))
      mat:Translate( Vector(-10,0,0) )
      self:EnableMatrix("RenderMultiply", mat)
      self:SetModelScale(1.5,0) --slightly fuzzy, looks better this way
      self:DrawModel()
      self:SetModelScale(1,0)
      render.SetBlend(1)
      render.SetStencilCompareFunction(STENCILCOMPARISONFUNCTION_EQUAL)
      cam.Start3D2D(pos,ang,1)
        surface.SetDrawColor(self:GetRed(),self:GetGreen(),self:GetBlue(),100)
        surface.DrawRect(-ScrW(),-ScrH(),ScrW()*2,ScrH()*2)
      cam.End3D2D()
    render.SetStencilEnable(false)
  end
end

function ENT:Think()
  local dlight = DynamicLight( self:EntIndex() )
	if ( dlight ) and self:GetDisplay() then
		dlight.pos = self:GetPos()
		dlight.r = self:GetRed()
		dlight.g = self:GetGreen()
		dlight.b = self:GetBlue()
		dlight.brightness = 1
		dlight.Decay = 1000
		dlight.Size = 100
		dlight.DieTime = CurTime() + 1
	end
end
