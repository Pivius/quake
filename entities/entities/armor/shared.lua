ENT.Base = "base_gmodentity"
ENT.Type = "anim"

function ENT:SetupDataTables()
	self:NetworkVar( "String", 0, "Type" )
  self:NetworkVar( "Vector", 1, "Origin" )
  self:NetworkVar( "Bool", 2, "Display" )
	self:NetworkVar( "Float", 3, "Cooldown" )
	self:NetworkVar( "Float", 4, "Red" )
	self:NetworkVar( "Float", 5, "Green" )
	self:NetworkVar( "Float", 6, "Blue" )
	self:NetworkVar( "Float", 7, "Alpha" )
end
