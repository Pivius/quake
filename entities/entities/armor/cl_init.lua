include('shared.lua')

function ENT:Draw()
--self:DrawModel()
if self:GetDisplay() then
  local pos = LocalPlayer():EyePos()+LocalPlayer():EyeAngles():Forward()*10
  local ang = LocalPlayer():EyeAngles()
  ang = Angle(ang.p+90,ang.y,0)
  render.ClearStencil()
  render.SetStencilEnable(true)
    render.SetStencilWriteMask(255)
    render.SetStencilTestMask(255)
    render.SetStencilReferenceValue(15)
    render.SetStencilFailOperation(STENCILOPERATION_KEEP)
    render.SetStencilZFailOperation(STENCILOPERATION_KEEP)
    render.SetStencilPassOperation(STENCILOPERATION_REPLACE)
    render.SetStencilCompareFunction(STENCILCOMPARISONFUNCTION_ALWAYS)
    render.SetBlend(0) --don't visually draw, just stencil
    scale = Vector(1,1,1)
    mat = Matrix()
    mat:Scale(Vector(scale))
    mat:Translate( Vector(0,0,0) )
    self:EnableMatrix("RenderMultiply", mat)
    self:SetModelScale(0.75,0) --slightly fuzzy, looks better this way
    self:DrawModel()
    self:SetModelScale(1,0)
    render.SetBlend(1)
    render.SetStencilCompareFunction(STENCILCOMPARISONFUNCTION_EQUAL)
    cam.Start3D2D(pos,ang,1)
      if self:GetType() == "Green" then
        surface.SetDrawColor(0,100,0,155)
        self:SetRed(0)
        self:SetGreen(100)
        self:SetBlue(0)
      elseif self:GetType() == "Yellow" then
        surface.SetDrawColor(100,100,0,155)
        self:SetRed(100)
        self:SetGreen(100)
        self:SetBlue(0)
      elseif self:GetType() == "Red" then
        surface.SetDrawColor(50,0,0,155)
        self:SetRed(50)
        self:SetGreen(0)
        self:SetBlue(0)
      end
      surface.DrawRect(-ScrW(),-ScrH(),ScrW()*2,ScrH()*2)
    cam.End3D2D()

    /*
    scale = Vector(1,1,1)
    mat = Matrix()
    mat:Scale(Vector(scale))
    mat:Translate( Vector(0,0,0) )
    self:EnableMatrix("RenderMultiply", mat)
    self:DrawModel()*/
  render.SetStencilEnable(false)

  render.ClearStencil()
  render.SetStencilEnable(true)
    render.SetStencilWriteMask(255)
    render.SetStencilTestMask(255)
    render.SetStencilReferenceValue(15)
    render.SetStencilFailOperation(STENCILOPERATION_KEEP)
    render.SetStencilZFailOperation(STENCILOPERATION_KEEP)
    render.SetStencilPassOperation(STENCILOPERATION_REPLACE)
    render.SetStencilCompareFunction(STENCILCOMPARISONFUNCTION_ALWAYS)
    render.SetBlend(0) --don't visually draw, just stencil
    scale = Vector(1.1,1.1,1.01)
    mat = Matrix()
    mat:Scale(Vector(scale))
    mat:Translate( Vector(0,0,0) )
    self:EnableMatrix("RenderMultiply", mat)
    self:SetModelScale(0.7525,0) --slightly fuzzy, looks better this way
    self:DrawModel()
    self:SetModelScale(1,0)
    render.SetBlend(1)
    render.SetStencilCompareFunction(STENCILCOMPARISONFUNCTION_EQUAL)
    cam.Start3D2D(pos,ang,1)
      if self:GetType() == "Green" then
        surface.SetDrawColor(0,255,0,20)
        self:SetRed(0)
        self:SetGreen(255)
        self:SetBlue(0)
      elseif self:GetType() == "Yellow" then
        surface.SetDrawColor(255,255,0,20)
        self:SetRed(255)
        self:SetGreen(255)
        self:SetBlue(0)
      elseif self:GetType() == "Red" then
        surface.SetDrawColor(255,0,0,50)
        self:SetRed(255)
        self:SetGreen(0)
        self:SetBlue(0)
      end
      surface.DrawRect(-ScrW(),-ScrH(),ScrW()*2,ScrH()*2)
    cam.End3D2D()

    /*
    scale = Vector(1,1,1)
    mat = Matrix()
    mat:Scale(Vector(scale))
    mat:Translate( Vector(0,0,0) )
    self:EnableMatrix("RenderMultiply", mat)
    self:DrawModel()*/
  render.SetStencilEnable(false)
end
end

function ENT:Think()
  local dlight = DynamicLight( self:EntIndex() )
	if ( dlight ) and self:GetDisplay() then
		dlight.pos = self:GetPos()
		dlight.r = self:GetRed()
		dlight.g = self:GetGreen()
		dlight.b = self:GetBlue()
		dlight.brightness = 1
		dlight.Decay = 1000
		dlight.Size = 100
		dlight.DieTime = CurTime() + 1
	end
end
