AddCSLuaFile('cl_init.lua')
AddCSLuaFile('shared.lua')

include('shared.lua')
local activated = false
function ENT:Initialize()


  self:SetModel("models/items/hevsuit.mdl")
  local min, max = self:GetModelBounds()
  self.min = min + self:GetPos()
  self.max = max + self:GetPos()
  local BBOX = (self.max - self.min)
	self:SetSolid( SOLID_BBOX )
	self:PhysicsInitBox( -BBOX, BBOX )
	self:SetCollisionBoundsWS( self.min, self.max )

	self:SetTrigger( true )
	self:DrawShadow( false )
	self:SetNotSolid( true )
	self:SetNoDraw( false )
  self:SetType( self.Type )
  self:SetOrigin( self:GetPos() )
  self:SetDisplay( true )
  self:SetCooldown( self.Cooldown )
/*
	self.Phys = self:GetPhysicsObject()
	if self.Phys and self.Phys:IsValid() then
		self.Phys:Sleep()
		self.Phys:EnableCollisions( false )
	end*/
end

function ENT:Think()
	local ang = self:GetAngles()
  ang:RotateAroundAxis(self:GetUp() , FrameTime()*64)
  self:SetAngles(ang)


end

function ENT:StartTouch(ent)
    if ent:IsPlayer() then
      if self:GetDisplay() then

        if self:GetType() == "Green" then
          if ent:GetArmorType() == "Red" and ent:GetArmour() <= 66 or
          ent:GetArmorType() == "Yellow" and ent:GetArmour() <= 75 or
          ent:GetArmorType() == "Green" and ent:GetArmour() < 100 or
          ent:GetArmorType() == "None" then
            self:SetDisplay( false )
            if (ent:GetArmour() + 50) > 100 then
              ent:SetArmour(100)
            else
              ent:SetArmour(ent:GetArmour() + 50)
            end
            ent:SetAbsorbtion(1/2)
            ent:SetArmorType("Green")
            timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
          end
        elseif self:GetType() == "Yellow" then
          if ent:GetArmorType() == "Red" and ent:GetArmour() <= 131 or
          ent:GetArmorType() == "Yellow" and ent:GetArmour() < 150 or
          ent:GetArmorType() == "Green" or
          ent:GetArmorType() == "None" then
            self:SetDisplay( false )
            if (ent:GetArmour() + 100) > 150 then
              ent:SetArmour(150)
            else
              ent:SetArmour(ent:GetArmour() + 100)
            end
            ent:SetAbsorbtion(1- 2/3)
            ent:SetArmorType("Yellow")
            timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
          end
        elseif self:GetType() == "Red" then
          if ent:GetArmorType() == "Red" and ent:GetArmour() < 200  or
          ent:GetArmorType() == "None" or
          ent:GetArmorType() == "Green" or
          ent:GetArmorType() == "Yellow" then
            self:SetDisplay( false )
            if (ent:GetArmour() + 150) > 200 then
              ent:SetArmour(200)
            else
              ent:SetArmour(ent:GetArmour() + 100)
            end
            ent:SetAbsorbtion(1/4)
            ent:SetArmorType("Red")
            timer.Simple(self:GetCooldown(), function() self:SetDisplay( true ) end)
          end
        end
      end
    end
end
