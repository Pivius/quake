--[[---------------------------------------------------------

  Sandbox Gamemode

  This is GMod's default gamemode

-----------------------------------------------------------]]

-- These files get sent to the client
AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( 'UtiliT.lua' )
AddCSLuaFile( 'getset.lua' )
AddCSLuaFile("cl_fonts.lua")
AddCSLuaFile( "persistence.lua" )
AddCSLuaFile( "shared.lua" )
AddCSLuaFile( 'cl_armor.lua' )
AddCSLuaFile( "cl_hud.lua" )
AddCSLuaFile( 'sh_movement.lua' )
AddCSLuaFile( 'sh_jumps.lua' )
AddCSLuaFile( "cl_sonar.lua")
AddCSLuaFile("cl_hitpoints.lua")
AddCSLuaFile( 'sh_editor.lua' )
AddCSLuaFile( 'cl_editor.lua' )
--AddCSLuaFile("pedit.lua")

include('sql/my_sql.lua')
include( 'UtiliT.lua' )
include( 'shared.lua' )
include( 'player.lua' )
include( 'getset.lua' )

include( 'sv_health.lua' )
include('sv_notifications.lua')
include('sql/database.lua')
include('sql/core.lua')
include( 'admin/sv_hooks.lua')
include( 'admin/sv_admin.lua')
include( 'admin/sv_commands.lua')
include('sv_player.lua')
include("sv_trails.lua")
include("sv_view.lua")
include("sv_armor.lua")
include("sv_sonar.lua")
include("sv_hitpoints.lua")
include("sv_editor.lua")
include( 'sh_editor.lua' )

include( 'sh_movement.lua' )
include( 'sh_jumps.lua' )

util.AddNetworkString( "Update" )
local version = "0.0.01"
local versiondate = "01.16.2017"

print("//////////////////////////////////////////////////////////////////////////")
print("///" .. string.rep(" ", 68) .. "///")
print("///" .. string.rep(" ", 25) .. "Created by Pivius" .. string.rep(" ", 26) .."///")
print("///" .. string.rep(" ", 27) .. "Version " .. version .. string.rep(" ", 33 - #version) .."///" )
print("///" .. string.rep(" ", 16) .. "This version was created " .. versiondate .. string.rep(" ", 27 - #versiondate) .."///")
print("///" .. string.rep(" ", 68) .. "///")
print("//////////////////////////////////////////////////////////////////////////")

--
-- Make BaseClass available
--
DEFINE_BASECLASS( "gamemode_base" )

--


hook.Add("Initialize", "console", function()
	game.ConsoleCommand("sv_maxvelocity 9999\n")
	game.ConsoleCommand( "sv_friction 8\n" )
	game.ConsoleCommand( "sv_gravity 800\n" )
	game.ConsoleCommand("sv_sticktoground 1\n")
	game.ConsoleCommand("sv_airaccelerate 0\n")
	game.ConsoleCommand("sv_accelerate 15\n")
	game.ConsoleCommand("mp_falldamage 0\n")
	game.ConsoleCommand("sv_stopspeed 75\n")
	game.ConsoleCommand("sv_friction 0\n")

	end)
	Admin.Commands:LoadCMD()
	Admin:Init()

hook.Add( "InitPostEntity", "LoadEntities", function()

end)


function GM:Think()
	for _, ply in pairs(player.GetAll()) do
		for k, wep in pairs(ply:GetWeapons()) do
			if ply:GetAmmoCount(wep:GetPrimaryAmmoType()) > 0 then
				wep:SetClip1(wep:Clip1() + ply:GetAmmoCount( wep:GetPrimaryAmmoType()))
		    ply:RemoveAmmo( ply:GetAmmoCount( wep:GetPrimaryAmmoType() ), wep:GetPrimaryAmmoType() )
		  end
		end
	end
end



function GM:GetFallDamage( ply, speed )
	--return math.max( 0, math.ceil( 0.2418*speed - 141.75 +5 ) )
end

hook.Add("PlayerDeath", "Die", function ( pl )
	pl:SetArmour(0)
	pl:SetAbsorbtion(1)
	pl:SetMega(false)
end)



function GM:OnDamagedByExplosion( ply, dmginfo )

			ply:SetDSP(34,false)
end
--[[---------------------------------------------------------
   Name: gamemode:PlayerSpawn( )
   Desc: Called when a player spawns
-----------------------------------------------------------]]
function GM:PlayerSpawn( pl )


	player_manager.SetPlayerClass( pl, "player_sandbox" )

	BaseClass.PlayerSpawn( self, pl )

	pl:SetAvoidPlayers( false )
	pl:SetCollisionGroup( COLLISION_GROUP_WEAPON )

end

function GM:EntityTakeDamage( target, dmginfo )

			if dmginfo:GetAttacker() == target and (dmginfo:GetInflictor( ):GetClass() == "send_rpg_missile") then
				dmginfo:ScaleDamage(0.5)
			else
				dmginfo:ScaleDamage(1)
			end
			if (dmginfo:GetInflictor( ):GetClass() == "weapon_rocketlauncher") then
				local pos = (dmginfo:GetDamagePosition()	+ Vector(0, 0, -440))

				local Force = target:GetPos() - pos
				local Dist = Force:Length()
				Force:Normalize()
				Force = Force  * math.Clamp(512 - Dist, 0, 512)

				target:SetVelocity(target:GetVelocity() + Force)
			end
end


--[[---------------------------------------------------------
   Name: gamemode:PlayerShouldTakeDamage
   Return true if this player should take damage from this attacker
   Note: This is a shared function - the client will think they can
	 damage the players even though they can't. This just means the
	 prediction will show blood.
-----------------------------------------------------------]]
function GM:PlayerShouldTakeDamage( ply, attacker )

	-- The player should always take damage in single player..
	if ( game.SinglePlayer() ) then return true end

	-- No player vs player damage
	if ( attacker:IsValid() && attacker:IsPlayer() ) then
		return cvars.Bool( "sbox_playershurtplayers", true )
	end
	-- Default, let the player be hurt
	return true

end

function GM:CreateEntityRagdoll( entity, ragdoll )
end

-- Set the ServerName every 30 seconds in case it changes..
-- This is for backwards compatibility only - client can now use GetHostName()
local function HostnameThink()

	SetGlobalString( "ServerName", GetHostName() )

end

timer.Create( "HostnameThink", 30, 0, HostnameThink )


--[[---------------------------------------------------------
   Called once on the player's first spawn
-----------------------------------------------------------]]
function GM:PlayerInitialSpawn( ply )

	ply:SetHullDuck(Vector(-16,-16,0), Vector(16,16,40))
	BaseClass.PlayerInitialSpawn( self, ply )
end
