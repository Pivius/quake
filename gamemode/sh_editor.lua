local meta = FindMetaTable( "Player" )
Editor = {} or Editor

function Editor:Init()
  GetSet("Noclip", "Player")

  local files = file.Find( "quake/gamemode/vgui/*.lua", "LUA" )
  local tbl = {}
  for _,v in pairs( files ) do
    if string.match(v, "sv_") then
      include( "quake/gamemode/vgui/" .. v )
    elseif string.match(v, "cl_") then
      if CLIENT then
        include( "quake/gamemode/vgui/" .. v )
      else
        AddCSLuaFile( "quake/gamemode/vgui/" .. v )
      end
    end
  end
end
Editor:Init()

hook.Add("PlayerInitialSpawn", function(ply)
  ply:SetNoclip(false)
end)

function meta:toggleNoclip(bool)
  local noclip = self:GetNoclip()
    if !bool then
      if noclip then
        self:SetNWBool( "Noclip", false )
        self:SetNoclip(false)
      else
        self:SetNWBool( "Noclip", true )
        self:SetNoclip(true)
      end
    else
      self:SetNoclip(bool)
    end
end
if CLIENT then
  function UpdateUI()
    -- Sync Client and Server
    for _, ply in pairs(player.GetAll()) do
      if ply:GetNWBool("Noclip") ~= ply:GetNoclip() then
        ply:SetNoclip(ply:GetNWBool("Noclip"))
        hook.Call("UpdateDraw")
      end
    end
  end
  hook.Add("Think", "Update Editor UI", UpdateUI)
end

function Noclip(ply, mv)

  -- Disable noclip
  if !ply:GetNoclip() then return false end

  local forward = mv:GetForwardSpeed()
  local side = mv:GetSideSpeed()
  local up = mv:GetUpSpeed()
  local vel = Vector()
  local ang = mv:GetMoveAngles()
  local speed = 0.1 * FrameTime()
  if ( mv:KeyDown( IN_SPEED ) ) then speed = 0.15 * FrameTime() end

  vel = vel + ang:Forward() * mv:GetForwardSpeed()* speed
	vel = vel + ang:Right() * mv:GetSideSpeed()* speed
	vel = vel + ang:Up() * mv:GetUpSpeed()* speed
	mv:SetOrigin( mv:GetOrigin() + (vel)  )
	mv:SetVelocity( vel )

	return true

end
hook.Add( "Move", "Noclip move", Noclip )
