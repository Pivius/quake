local meta = FindMetaTable( "Player" )

function noDamage( target, dmginfo )
	dmginfo:ScaleDamage(0)
end
hook.Add("EntityTakeDamage", "Take no damage", noDamage)
