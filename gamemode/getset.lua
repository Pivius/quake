/*---------------
  NAME - GetSet
	FUNCTION - Creates a getter and a setter for a attribute
*/---------------
function GetSet(attr, meta)
	if !attr or !meta then return end
	mt = FindMetaTable( meta )
	local copy = getmetatable(attr)

	local GS ={

		["Get" .. attr] = function( self )
			return self[ attr ]
		end,

		["Set" .. attr] = function( self, var )
			self[ attr ] = var
		end
	}
	table.Merge(copy, GS)

	GS.__index = copy

	setmetatable( mt, GS )

end

/*---------------
  NAME - removeGetSet
	FUNCTION - Removes a getter and a setter for a attribute
*/---------------
function removeGetSet(attr, meta)
	if !attr or !meta then return end
	mt = FindMetaTable( meta )
	local GS = getmetatable(attr)
	PrintTable(GS)
	print()
	if UT:HasKey(GS, "Get" .. attr) and UT:HasKey(GS, "Set" .. attr) then
		UT:RemoveByKey(GS, "Get" .. attr)
		UT:RemoveByKey(GS, "Set" .. attr)
		GS.__index = GS
		setmetatable( mt, GS )
	end
end
