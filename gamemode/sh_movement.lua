local meta = FindMetaTable( "Player" )
movement = {}

function movement:Init()
  GetSet("Friction", "Player")
  meta:SetFriction(8)
end
movement:Init()

-- Detect Surfing
function isOnRamp( player )
  if !IsFirstTimePredicted() then return end
  local pos = player:GetPos()
  local mins = player:OBBMins()
  local maxs = player:OBBMaxs()
  local endPos = pos * 1
  endPos.z = endPos.z -1

  local tr = util.TraceHull{
    start = pos,
    endpos = endPos,
    mins = mins,
    maxs = maxs,
    mask = MASK_PLAYERSOLID_BRUSHONLY,
    filter = function(e1, e2)
      return not e1:IsPlayer()
    end
  }
  if(tr.Fraction ~= 1) then
  -- Gets the normal vector of the surface under the player
    local Plane = tr.HitNormal
  -- Make sure it's not flat ground and not a surf ramp (1.0 = flat ground, < 0.7 = surf ramp)
    if(0.2 <= Plane.z and Plane.z < 1) then
			return true
		else
		  return false
    end
	else
		return false
  end
end

function ClipVelocity(vecin, normal, vecout, overbounce)
	local	change
	local backoff
  backoff = vecin:Dot(normal)*overbounce

    for i=1,3, 1 do
      change  = normal[i] * backoff
      vecout[i] = vecin[i] - change
    end


  local adjust = vecout:Dot(normal)
  if (adjust < 0.0) then
    vecout.x = vecout.x - (normal.x * adjust)
    vecout.y = vecout.y - (normal.y * adjust)
    vecout.z = vecout.z - (normal.z * adjust)

  end

end

//https://github.com/ValveSoftware/source-sdk-2013/blob/master/mp/src/game/shared/gamemovement.cpp#L1612
function Friction(ply, mv)
  local vel        = mv:GetVelocity()
	local	speed      = vel:Length()
  local newspeed
  local control
	local	friction
	local	drop

	// If too slow, return
	if (speed < 0.1) then
		return
  end
  drop = 0
	// apply ground friction
  --if SERVER then
	if (ply:OnGround()) then
    friction = ply:GetFriction()
    // On an entity that is the ground
 		// Bleed off some speed, but if we have less than the bleed
		//  threshold, bleed the threshold amount.
    if (speed < GetConVar("sv_stopspeed"):GetFloat()) then
      control = GetConVar("sv_stopspeed"):GetFloat()
    else
      control = speed
    end
		// Add the amount to the drop amount.
    drop = drop + (control*friction*FrameTime())
  end
--end

  	// scale the velocity
  	newspeed = speed - drop
  	if (newspeed < 0) then
  		newspeed = 0
    end

    if newspeed != speed then
    	// Determine proportion of old speed we are using.
    	newspeed = newspeed / speed


      // Maintain speed while bhopping
      if mv:KeyDown(IN_JUMP) and ply:GetCanHop() then
        vel = mv:GetVelocity()

      else
        // Adjust velocity according to proportion.

        vel.x = vel.x * newspeed
        vel.y = vel.y * newspeed
        vel.z = vel.z * newspeed
      end
    end
    // Set the velocity
    mv:SetVelocity(vel)
end
hook.Add("SetupMove", "Friction", Friction)

function Accelerate( ply, mv )
  if !ply:OnGround() then return end
  local style = 1
  local accel = 0.001
  local vel   = mv:GetVelocity()
  local wishdir = Vector(0,0,0)
  local aim = mv:GetMoveAngles():Forward()
  local forward = Vector(aim.x,  aim.y, 0):GetNormal()
  local right	= Vector(aim.y, -aim.x, 0):GetNormal()
  wishdir = wishdir +forward *(mv:GetForwardSpeed()/20)
  wishdir = wishdir +right *(mv:GetSideSpeed()/20)

  wishdir = wishdir:GetNormal()
  local wishspeed	= wishdir:Length()*364
  /*
  if wishspeed > 873 then
    wishspeed = 873
  end*/
  if style == 1 then
  	// q2 style
    local currentspeed = vel:Dot(wishdir)
    local addspeed     = wishspeed - currentspeed
  	if (addspeed <= 0) then return end
  	local accelspeed    = accel*wishspeed
  	if (accelspeed > addspeed) then
  		accelspeed = addspeed
  	end
    if !mv:KeyPressed(IN_JUMP) and ply:GetCanHop() then
      mv:SetVelocity(mv:GetVelocity() + (accelspeed*wishdir))
    end
  else
  	// proper way (avoids strafe jump maxspeed bug), but feels bad
  	local wishVelocity = Vector( wishdir.x*wishspeed, wishdir.y*wishspeed, wishdir.z*wishspeed )
  	local pushDir = Vector( wishVelocity.x-vel.x, wishVelocity.y-vel.y, wishVelocity.z-vel.z )
  	local pushLen = pushDir:GetNormalized()
  	local canPush = accel*wishspeed
  	if (canPush > pushLen:Length()) then
  		canPush = pushLen:Length()
  	end
    if !mv:KeyPressed(IN_JUMP) and ply:GetCanHop() then
      mv:SetVelocity(vel + (pushDir*canPush))
    end
  end
end
hook.Add("FinishMove", "Accelerate", Accelerate)

function InvulnerabilityMove( cmd )
  cmd:SetForwardSpeed(0)
  cmd:GetSideSpeed(0)
  cmd:GetUpSpeed(0)
  cmd:SetVelocity(Vector(0,0,0))
end
local prevangle = Vector(0,0,0)
function AirMove( ply, mv, cud )
  local	wishdir   = Vector()
  local aim       = mv:GetMoveAngles():Forward()
  local forward   = Vector(aim.x,  aim.y, 0):GetNormal()
  local right	    = Vector(aim.y, -aim.x, 0):GetNormal()
  local vel       = mv:GetVelocity()
  local forwardir = Vector(-ply:GetAimVector():Angle():Right().y, ply:GetAimVector():Angle():Right().x,0)
  local curspeed  = Vector(vel.x, vel.y, 0)
  local curangle  = mv:GetAngles()
  forwardir       = forwardir:GetNormal()
  wishdir         = wishdir + forward * mv:GetForwardSpeed()
  wishdir         = wishdir + right * mv:GetSideSpeed()
	wishdir.z       = 0
  local wishspeed	= wishdir:Length()
  wishdir         = wishdir:GetNormal()

  if ply:OnGround() then return end
    local Accelerate = 0
    local speedCap = 0

    --Quake 3 Strafes/Strafe jumping
    if ( ply:KeyDown( IN_MOVELEFT ) and ply:KeyDown( IN_FORWARD ) and !ply:KeyDown( IN_MOVERIGHT ) and !ply:KeyDown( IN_BACK ) )
    or ( ply:KeyDown( IN_MOVERIGHT ) and ply:KeyDown( IN_FORWARD ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_BACK ) )
    or ( ply:KeyDown( IN_MOVERIGHT ) and ply:KeyDown( IN_BACK ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_FORWARD ) )
    or ( ply:KeyDown( IN_MOVELEFT ) and ply:KeyDown( IN_BACK ) and !ply:KeyDown( IN_MOVERIGHT ) and !ply:KeyDown( IN_FORWARD ) ) then
      Accelerate = 0.025
      speedCap = 300
    end

    --Regular airstrafe
    if ( ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_MOVERIGHT ) and !ply:KeyDown( IN_FORWARD ) and !ply:KeyDown( IN_BACK ) )
    or ( ply:KeyDown( IN_MOVERIGHT ) and  !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_FORWARD ) and !ply:KeyDown( IN_BACK ))
    or (ply:KeyDown( IN_FORWARD ) and !ply:KeyDown( IN_MOVERIGHT ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_BACK ))
    or (ply:KeyDown( IN_BACK ) and !ply:KeyDown( IN_MOVERIGHT ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_FORWARD )) then
      Accelerate = 0.4
      speedCap = 50
    end

    if vel:Length() <= 320 then
      Accelerate = 0.05
      speedCap = 320
    end
    if (wishspeed > speedCap) then
      wishspeed = speedCap
    end
    local currentspeed = vel:Dot(wishdir)
    local addspeed     = wishspeed - currentspeed
    if (addspeed <= 0) then return end
    local accelspeed    = Accelerate*wishspeed
    if (accelspeed > addspeed) then
      accelspeed = addspeed
    end
    mv:SetVelocity(mv:GetVelocity() + (accelspeed*wishdir))

end
hook.Add("FinishMove", "AirMove", AirMove)

local prevangle = Vector(0,0,0)
function WStrafing(ply, mv, cud)
	if (ply:Alive()) then
    local	wishdir   = Vector()
    local aim       = mv:GetMoveAngles():Forward()
    local forward   = Vector(aim.x,  aim.y, 0):GetNormal()
    local right	    = Vector(aim.y, -aim.x, 0):GetNormal()
    local vel       = mv:GetVelocity()
    local forwardir = Vector(-ply:GetAimVector():Angle():Right().y, ply:GetAimVector():Angle():Right().x,0)
    local curspeed  = Vector(vel.x, vel.y, 0)
    local curangle  = mv:GetAngles()
    if (((vel:Angle() - curangle):Right()).y *-1) < 0.55 then
		else
      if prevangle.y < curangle.y then
        aim = mv:GetMoveAngles():Forward() *-1
      else
        aim = mv:GetMoveAngles():Forward()
      end
      forward	= Vector(aim.y,  -aim.x, 0):GetNormal()
      wishdir = wishdir + forward * mv:GetForwardSpeed()
    end
		prevangle = curangle

    forwardir       = forwardir:GetNormal()
    wishdir         = wishdir + forward * mv:GetForwardSpeed()
    wishdir         = wishdir + right * mv:GetSideSpeed()
  	wishdir.z       = 0

		local wishspeed	= wishdir:Length()
		wishdir         = wishdir:GetNormal()
		if (wishspeed > 10) then
			wishspeed = 10
		end

		/*
local angle = ( mv:GetAngles() - mv:GetVelocity():Angle()  )
angle:Normalize()*/

		currentspeed = vel:Dot(wishdir)
		addspeed     = wishspeed - currentspeed
		if !ply:IsOnGround() then
			if (ply:KeyDown( IN_FORWARD ) and !ply:KeyDown( IN_MOVERIGHT ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_BACK ))
      or (ply:KeyDown( IN_BACK ) and !ply:KeyDown( IN_MOVERIGHT ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_FORWARD )) then
				if !(addspeed <= 0) then
	    	  local accelspeed = wishspeed*7
	    	  local addspeed = wishspeed - currentspeed --* 1.5
	    	  if (accelspeed > addspeed) then
	    	    accelspeed = addspeed
	    	  end
	    	  mv:SetVelocity(vel + (accelspeed * wishdir))
	    	end
			end

		end
	end
end
hook.Add("SetupMove", "W Strafing", WStrafing)

/*----

-- Strafe Jumping
function Q3Strafing(ply, cmd, cud)
	if (ply:Alive()) then
		local aim = cmd:GetMoveAngles():Forward()

		local AIR_SPEEDCAP	= 150
		local AIR_ACCEL	= 0.2
		if isOnRamp(ply) and !ply:IsOnGround() then
			AIR_SPEEDCAP	= 175
			AIR_ACCEL	= 0.2
		else
			AIR_SPEEDCAP	= 150
			AIR_ACCEL	= 0.2
		end
		local velocity = cmd:GetVelocity()
		local forward	= Vector(aim.x, aim.y, 0):GetNormal()
		local right	= Vector(aim.y, -aim.x, 0):GetNormal()
		local wishDir = Vector(0,0,0)
		local wishSpeed	= 0
		local addSpeed = 0
		local currentSpeed = 0

		wishDir 			= wishDir + forward * cmd:GetForwardSpeed()
		wishDir 			= wishDir + right * cmd:GetSideSpeed()

		wishSpeed			= wishDir:Length()
		wishDir				= wishDir:GetNormal()

		if (wishSpeed > AIR_SPEEDCAP) then
			wishSpeed = AIR_SPEEDCAP
		end

		currentSpeed = velocity:Dot(wishDir)
		addSpeed = wishSpeed - currentSpeed
		if !ply:IsOnGround() then
			if ( ply:KeyDown( IN_MOVELEFT ) and ply:KeyDown( IN_FORWARD ) and !ply:KeyDown( IN_MOVERIGHT ) and !ply:KeyDown( IN_BACK ) )
			or ( ply:KeyDown( IN_MOVERIGHT ) and ply:KeyDown( IN_FORWARD ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_BACK ) )
			or ( ply:KeyDown( IN_MOVERIGHT ) and ply:KeyDown( IN_BACK ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_FORWARD ) )
			or ( ply:KeyDown( IN_MOVERIGHT ) and ply:KeyDown( IN_BACK ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_FORWARD ) ) then

				if !(addSpeed <= 0) then
	    	  local accelSpeed = AIR_ACCEL * wishSpeed
	    	  local addSpeed = wishSpeed - currentSpeed --* 1.5
	    	  if (accelSpeed > addSpeed) then
	    	    accelSpeed = addSpeed
	    	  end
	    	  cmd:SetVelocity(velocity + (accelSpeed * wishDir))
	    	end
			end
		end
	end
end
hook.Add("SetupMove", "Quake3 Strafing", Q3Strafing)

function ForwardAccel(ply, cmd, cud)
  local velocity     = cmd:GetVelocity()
  local forwardir = Vector(-ply:GetAimVector():Angle():Right().y, ply:GetAimVector():Angle():Right().x,0)
  local curspeed = Vector(velocity.x, velocity.y, 0)
  forwardir = forwardir:GetNormal()
  if !ply:IsOnGround() then
    if (ply:KeyDown( IN_FORWARD ) and !ply:KeyDown( IN_MOVERIGHT ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_BACK ))
    or ( ply:KeyDown( IN_MOVELEFT ) and ply:KeyDown( IN_FORWARD ) and !ply:KeyDown( IN_MOVERIGHT ) and !ply:KeyDown( IN_BACK ) )
    or ( ply:KeyDown( IN_MOVERIGHT ) and ply:KeyDown( IN_FORWARD ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_BACK ) ) then
      if curspeed:Length() > 320 then
        forwardir = Vector(0,0,0)
      end
      cmd:SetVelocity(velocity + (10 * forwardir))
    elseif (ply:KeyDown( IN_BACK ) and !ply:KeyDown( IN_MOVERIGHT ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_FORWARD ))
    or ( ply:KeyDown( IN_MOVERIGHT ) and ply:KeyDown( IN_BACK ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_FORWARD ) )
    or ( ply:KeyDown( IN_MOVERIGHT ) and ply:KeyDown( IN_BACK ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_FORWARD ) ) then

      if curspeed:Length() > 320 then
        forwardir = Vector(0,0,0)
      end
      cmd:SetVelocity(velocity + (10 * -forwardir))
    end
  end
end
hook.Add("SetupMove", "ForwardAccel", ForwardAccel)

--Air Control Strafes
local prevangle = Vector(0,0,0)
function WStrafing(ply, cmd, cud)
	if (ply:Alive()) then
		local aim             = cmd:GetMoveAngles():Forward()
		local curangle        = cmd:GetAngles()
		local AIR_SPEEDCAP  	= 5
		local AIR_ACCEL	      = 50
		if isOnRamp(ply) and !ply:IsOnGround() then
			AIR_SPEEDCAP = 5
			AIR_ACCEL	   = 10
		else
			AIR_SPEEDCAP = 5
			AIR_ACCEL	   = 50
		end
		local velocity     = cmd:GetVelocity()
		local forward      = Vector(aim.y,  -aim.x, 0):GetNormal()
    local right	       = Vector(aim.y, -aim.x, 0):GetNormal()
		local wishDir      = Vector(0,0,0)
		local wishSpeed	   = 0
		local addSpeed     = 0
		local currentSpeed = 0
    if (((velocity:Angle() - curangle):Right()).y *-1) < 0.85 then
      AIR_SPEEDCAP	= 30
			AIR_ACCEL	    = 1
      forward	      = Vector(aim.x,  aim.y, 0):GetNormal()
      wishDir       = wishDir + forward * cmd:GetForwardSpeed()
      wishDir       = wishDir + right * cmd:GetSideSpeed()
		else
      AIR_SPEEDCAP	= 5
      AIR_ACCEL	    = 50
      if prevangle.y < curangle.y then
        aim = cmd:GetMoveAngles():Forward() *-1
      else
        aim = cmd:GetMoveAngles():Forward()
      end
      forward	= Vector(aim.y,  -aim.x, 0):GetNormal()
      wishDir = wishDir + forward * cmd:GetForwardSpeed()
    end
		prevangle = curangle


		wishSpeed			= wishDir:Length()
		wishDir				= wishDir:GetNormal()
		if (wishSpeed > AIR_SPEEDCAP) then
			wishSpeed = AIR_SPEEDCAP
		end
    */--------------


		/*
local angle = ( cmd:GetAngles() - cmd:GetVelocity():Angle()  )
angle:Normalize()*/
/*------------------------
		currentSpeed = velocity:Dot(wishDir)
		addSpeed     = wishSpeed - currentSpeed
		if !ply:IsOnGround() then
			if (ply:KeyDown( IN_FORWARD ) and !ply:KeyDown( IN_MOVERIGHT ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_BACK ))
      or (ply:KeyDown( IN_BACK ) and !ply:KeyDown( IN_MOVERIGHT ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_FORWARD )) then
				if !(addSpeed <= 0) then
	    	  local accelSpeed = wishSpeed - currentSpeed
	    	  local addSpeed = wishSpeed - currentSpeed --* 1.5
	    	  if (accelSpeed > addSpeed) then
	    	    accelSpeed = addSpeed
	    	  end
	    	  cmd:SetVelocity(velocity + (accelSpeed * wishDir))
	    	end
			end

		end
	end
end
hook.Add("SetupMove", "W Strafing", WStrafing)

-- Bunnyhopping
function AirStrafing(ply, cmd, cud)
	if (ply:Alive()) then
		local aim = cmd:GetMoveAngles():Forward()
		local AIR_SPEEDCAP	= 30
		local AIR_ACCEL	= 1
		if isOnRamp(ply) and !ply:IsOnGround() then
			AIR_SPEEDCAP	= 50
			AIR_ACCEL	= 1000
		else
			AIR_SPEEDCAP	= 30
			AIR_ACCEL	= 1
		end

		local velocity = cmd:GetVelocity()
		local forward	= Vector(aim.x, aim.y, 0):GetNormal()
		local right	= Vector(aim.y, -aim.x, 0):GetNormal()
		local wishDir = Vector(0,0,0)
		local wishSpeed	= 0
		local addSpeed = 0
		local currentSpeed = 0

		wishDir 			= wishDir + forward * cmd:GetForwardSpeed()
		wishDir 			= wishDir + right * cmd:GetSideSpeed()

		wishSpeed			= wishDir:Length()
		wishDir				= wishDir:GetNormal()

		if (wishSpeed > AIR_SPEEDCAP) then
			wishSpeed = AIR_SPEEDCAP
		end

		currentSpeed = velocity:Dot(wishDir)
		addSpeed = wishSpeed - currentSpeed
		if !ply:IsOnGround() then
			if ( ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_MOVERIGHT ) and !ply:KeyDown( IN_FORWARD ) and !ply:KeyDown( IN_BACK ) )
			or ( ply:KeyDown( IN_MOVERIGHT ) and  !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_FORWARD ) and !ply:KeyDown( IN_BACK ))
      or (ply:KeyDown( IN_FORWARD ) and !ply:KeyDown( IN_MOVERIGHT ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_BACK ))
			or (ply:KeyDown( IN_BACK ) and !ply:KeyDown( IN_MOVERIGHT ) and !ply:KeyDown( IN_MOVELEFT ) and !ply:KeyDown( IN_FORWARD )) then

				if !(addSpeed <= 0) then
	    	  local accelSpeed = AIR_ACCEL * wishSpeed
	    	  local addSpeed = wishSpeed - currentSpeed --* 1.5
	    	  if (accelSpeed > addSpeed) then
	    	    accelSpeed = addSpeed
	    	  end
	    	  cmd:SetVelocity(velocity + (accelSpeed * wishDir))
	    	end
			end
		end
	end
end
hook.Add("SetupMove", "Strafing", AirStrafing)
*/--------------
