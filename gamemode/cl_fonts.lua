function InitalizeFonts()
  surface.CreateFont("HUD Health", {
    font = "Acens",
    size = 40
  })
  surface.CreateFont("HUD cross", {
    font = "MavenPro-Black",
    size = 35
  })
  surface.CreateFont("HUD Ammo", {
    font = "Acens",
    size = 30
  })
  surface.CreateFont("HUD HitPoints", {
    font = "Acens",
    size = 25
  })

  surface.CreateFont("Editor Text", {
    font = "MavenPro-Black",
    size = 25
  })
end
