Editor.button = {} or Editor.button

function Editor.button:Create()
 self.panel = vgui.Create( "DModelPanel" )

 self.panel:SetAlpha(0)
 self.panel:SetColor(self.color)
 self.panel:SetSize( 0, 0 )
 self.panel:SetPos(0, 0)

 if !self.parent then
   self.p = vgui.Create( "DFrame" )
 else
   self.p = vgui.Create( "DFrame", self.parent )
 end
 self.p:SetSize( self.width, self.height )
 self.p:SetPos(self.X, self.Y)
 self.p:SetAlpha(255)
 self.p:SetDraggable(false)
 self.p:ShowCloseButton( false )
 self.p:SetTitle( "" )
 /*
 if !self.parent then
   self.button = vgui.Create( "DButton" )
 else*/
   self.button = vgui.Create( "DButton", self.p )
 --end
 self.button:SetSize( self.width, self.height )
 self.button:SetPos(0, 0)
 self.button:Dock( self.DOCK ) -- top
 self.button:DockMargin( self.mrg1, self.mrg2, self.mrg3, self.mrg4 ) -- 0,0,0,0.1
 self.button:SetAlpha(0)
 self.button:SetText("")

 self.effect = false

 self.button.OnCursorEntered = function()
     self.panel:ColorTo(self.highlight, 0.15)
     self.effect = true
     self.chevsmooth = -25
     self.chevalpha = 255
 end

 self.button.OnCursorExited = function()
     self.panel:ColorTo(self.color, 0.15)
     self.effect = false
     self.p.Paint = function( )
     end
 end

 self.txtw, self.txth = surface.GetTextSize(self.Text)

 self.p.Think = function()
   if self.effect == true then
     self.p.Paint = function( )
       if math.Round(self.chevsmooth) == 200 then
          self.chevsmooth = -25
          self.chevalpha = 255
       end
       self.chevsmooth = self.chevsmooth and UT:Lerp(0.01, self.chevsmooth , 250) or 250
       self.chevalpha = self.chevalpha and UT:Lerp(0.045, self.chevalpha , 0) or 0
       Editor.chevron:Create(self.chevsmooth, 1.55, 0.171, Color(255,255,255,self.chevalpha))
     end
   end
 end

 self.p.Paint = function( )

 end

 self.button.Paint = function( )
  Editor.box:Create( 1, 1,
  self.width,
     self.height,
     self.panel:GetColor(),
     self.Text,
     ((self.width - self.txtw)/2),
     ((self.height - self.txth)/2),
     Color(255,255,255,255),
     self.font)
 end
 self.button.DoClick = function()
  self.funct = self.func()

 end
end

function Editor.button:Remove()
  if self.panel then
    self.panel:Remove()
  end
  --if self.parent then return end
  if self.button then
    self.button:Remove()
  end
  if self.p then
    self.p:Remove()
  end
end

function Editor.button:Funct(func)
 if !func then self.func = (function() end) return end
 self.func = func
end

function Editor.button:SetDock(dock)
 if !dock then self.DOCK = NODOCK return end
 self.DOCK = dock
 --self.button:Dock( self.DOCK )
end

function Editor.button:SetMargin(mrg1, mrg2, mrg3, mrg4)
 if !mrg1 then  return end
 self.mrg1 = mrg1
 self.mrg2 = mrg2
 self.mrg3 = mrg3
 self.mrg4 = mrg4


 --self.button:DockMargin( mrg1, mrg2, mrg3, mrg4 )
end

function Editor.button:SetTxt(text)
 if !text then print("No text") self.Text = "" return end
 self.Text = text
end

function Editor.button:SetFont(font)
 if !font then print("No font") self.font = "Editor Text" return end
 self.font = font
end

function Editor.button:SetParent(pnl)
  self.parent = pnl
end

function Editor.button:SetSize(w,h)
 if not w or not h then print("No scaling") return end
 self.width = w
 self.height = h
 if self.panel and self.button then
   self.button:SetSize(self.width, self.height)
 end
end

function Editor.button:GetSize()
 return self.width, self.height
end

function Editor.button:NewColor(vec)
 if not vec then print("No coloring") return end
 self.color = vec
end

function Editor.button:NewHighlight(vec)
 if not vec then print("No highlight") return end
 self.highlight = vec
end

function Editor.button:NewFlash(vec)
 if not vec then print("No flashing") return end
 self.flash = vec
end

function Editor.button:SetPos(x,y)
 if not x or not y then return end
 self.X = x
 self.Y = y
 if self.button and self.p then
   --self.button:SetPos(x, y)
   self.p:SetPos(x, y)
 end
end

function Editor.button:GetPos()
 return self.X, self.Y
end

function Editor.button:Move( frac, newpos, dir )
  if !dir then dir = "x" end
  local pos
  self.button.Think = function()
    self.smooth = self.smooth and UT:Lerp(frac, self.smooth , newpos) or newpos
    if dir == "x" then
      if self.X > newpos then
        pos = math.Clamp(self.smooth, newpos, self.X)
      elseif self.X < newpos then

        pos = math.Clamp(self.smooth, self.X, newpos)
      end
      self:SetPos( pos, self.Y )
    elseif dir == "y" then
      if self.Y > newpos then
        pos = math.Clamp(self.smooth, newpos, self.Y)
      elseif self.Y < newpos then

        pos = math.Clamp(self.smooth, self.Y, newpos)
      end
      self:SetPos( self.X, pos )
    end

  end
end

function Editor.button:Show( alpha, dur, delay, slidepos, dir )
  self:Move(0.1, slidepos, dir )
  if alpha > 0 then
    self.button:AlphaTo(alpha, dur, delay)
  else return end
end

function Editor.button:Hide( dur, delay, slidepos, dir )
  self:Move( 0.05, slidepos, dir )
  self.button:AlphaTo(0, dur, delay)
end


function Editor.button:Init()

 -- self:SetPos(0,0)
 self:SetSize(0,0)
 self:SetFont("Editor Text")
 self:SetDock(NODOCK)
 self:SetMargin(0, 0, 0, 0.1)
 self:NewColor(Color(255,255,255,150))
 self:NewHighlight(Color(255,255,255,255))
 self:NewFlash(Color(255,255,255,0))
 self:Funct(function() end)
 self:SetTxt("Init")
end


vgui.Register( "Button", Editor.button, "EditablePanel" )
