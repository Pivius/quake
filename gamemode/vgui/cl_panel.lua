Editor.panel = {} or Editor.panel

--Editor Panel
function Editor.panel:Create()
  self.blur = self.blur or {}
  self.blur = vgui.Create( "DFrame" )
  self.blur:SetDraggable(false)
  self.blur:ShowCloseButton( false )
  self.blur:SetTitle( "" )
  self.blur:SetAlpha(0)


  self.bg = self.bg or {}
  self.bg = vgui.Create( "DFrame" )
  self.bg:SetDraggable(false)
  self.bg:ShowCloseButton( false )
  self.bg:SetTitle( "" )
  self.bg:SetAlpha(0)

  self.blur:SetPos(self.X, self.Y)
  self.blur:SetSize(self.width, self.height)

  self.bg:SetPos(self.X, self.Y)
  self.bg:SetSize(self.width, self.height)


  self.blur.Paint = function()
    local blurTex = Material( "pp/blurscreen" )
    surface.SetMaterial( blurTex )
    surface.SetDrawColor( 255, 255, 255, self.blur:GetAlpha() )
    render.UpdateScreenEffectTexture()

      blurTex:SetFloat( "$blur", (1 / 3 ) * ( 5 ) )

      blurTex:Recompute()

      render.UpdateScreenEffectTexture()
      local xp, yp = self:GetPos()
      surface.DrawTexturedRect( -xp, -yp, ScrW(), ScrH() )

  end

  self.bg.Paint = function( )
        --create panel
        surface.SetDrawColor(self.color)
        surface.DrawRect(0, 0, self.width, self.height)

  end
end

function Editor.panel:Remove()
  if self.bg then
    self.bg:Remove()
  end
  if self.blur then
    self.blur:Remove()
  end
end


function Editor.panel:SetSize(w,h)
  if not w or not h then print("No scaling") return end
  self.width = w
  self.height = h
end

function Editor.panel:GetSize()
  local w, h = self.width, self.height
 return w, h
end

function Editor.panel:SetCol(vec)
  if not vec then print("No coloring") return end
  self.color = vec
end

function Editor.panel:SetPos(x,y)
  if not x or not y then return end
  self.X = x
  self.Y = y
  if self.bg and self.blur then
    self.bg:SetPos(x, y)
    self.blur:SetPos(x, y)
  end
end

function Editor.panel:GetPos()
  return self.X, self.Y
end

function Editor.panel:Move( frac, newpos, dir )
  if !dir then dir = "x" end
  local pos
  self.blur.Think = function()
    self.smooth = self.smooth and UT:Lerp(frac, self.smooth , newpos) or newpos
    if dir == "x" then
      if self.X > newpos then
        pos = math.Clamp(self.smooth, newpos, self.X)
      elseif self.X < newpos then

        pos = math.Clamp(self.smooth, self.X, newpos)
      end
      self:SetPos( pos, self.Y )
    elseif dir == "y" then
      if self.Y > newpos then
        pos = math.Clamp(self.smooth, newpos, self.Y)
      elseif self.Y < newpos then

        pos = math.Clamp(self.smooth, self.Y, newpos)
      end
      self:SetPos( self.X, pos )
    end

  end
end

function Editor.panel:Show( alpha, dur, delay, slidepos, dir )
  self:Move(0.1, slidepos, dir )
  if alpha > 0 then
    self.blur:AlphaTo(255, dur, delay)
    self.bg:AlphaTo(alpha, dur, delay)
  else return end
end

function Editor.panel:Hide( dur, delay, slidepos, dir )
  self:Move( 0.05, slidepos, dir )
  self.blur:AlphaTo(0, dur, delay)
  self.bg:AlphaTo(0, dur, delay)
end

function Editor.panel:Init()
  self:SetPos(0,0)
  self:SetSize(0,0)
  self:SetCol(Color(0,0,0,0))

end

vgui.Register( "Editor panel", Editor.panel, "EditablePanel" )
