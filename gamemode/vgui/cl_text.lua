Editor.text = {} or Editor.text


 function Editor.text:Create()
   if !self.parent then
     self.text = vgui.Create( "DLabelEditable" )
   else
     self.text = vgui.Create( "DLabelEditable", self.parent )
   end

   self.text:SetSize( self.width, self.width )
   self.text:SetPos(self.X, self.Y)
   self.text:SetAlpha(0)
   self.text:SetColor(self.color)
   self.text:SetText( "" )

   self.text.Paint = function()
     Editor.box:Create( 1, 1, 0, 0, self.color, self.Text, 1, 1, Color(255,255,255), self.font)
   end

 end

function Editor.text:Remove()
  if self.text then
    self.text:Remove()
  end
end

function Editor.text:SetFont(font)
  if !font then print("No font")return end
  self.font = font
end

function Editor.text:SetTxt(text)
  if !text then print("No text")return end
  self.Text = text
end

function Editor.text:SetSize(w,h)
  if not w or not h then print("No scaling") return end
  self.width = w
  self.height = h
  if self.text then
    self.text:SetSize(self.width, self.height)
  end
end

function Editor.text:GetSize()
  return self.width, self.height
end

function Editor.text:SetCol(vec)
  if not vec then print("No coloring") return end
  self.color = vec
end

function Editor.text:SetPos(x,y)
  if not x or not y then return end
  self.X = x
  self.Y = y
  if self.text then
    self.text:SetPos(self.X, self.Y)
  end
end
function Editor.text:SetParent(pnl)
  self.parent = pnl
end

function Editor.text:GetPos()
  return self.X, self.Y
end

function Editor.text:Move( frac, newpos, dir )
  if !dir then dir = "x" end
  local pos
  self.text.Think = function()
    self.smooth = self.smooth and UT:Lerp(frac, self.smooth , newpos) or newpos
    if dir == "x" then
      if self.X > newpos then
        pos = math.Clamp(self.smooth, newpos, self.X)
      elseif self.X < newpos then

        pos = math.Clamp(self.smooth, self.X, newpos)
      end
      self:SetPos( pos, self.Y )
    elseif dir == "y" then
      if self.Y > newpos then
        pos = math.Clamp(self.smooth, newpos, self.Y)
      elseif self.Y < newpos then

        pos = math.Clamp(self.smooth, self.Y, newpos)
      end
      self:SetPos( self.X, pos )
    end

  end
end

function Editor.text:Show( alpha, dur, delay, slidepos, dir )
  self:Move(0.1, slidepos, dir )
  if alpha > 0 then
    self.text:AlphaTo(alpha, dur, delay)
  else return end
end

function Editor.text:Hide( dur, delay, slidepos, dir )
  self:Move( 0.05, slidepos, dir )
  self.text:AlphaTo(0, dur, delay)
end

function Editor.text:Init()
  self:SetPos(0,0)
  self:SetSize(0,0)
  self:SetTxt("Init")
  self:SetFont("Editor Text")
  self:SetCol(Color(0, 0, 0, 0))
end

vgui.Register( "Text", Editor.text, "EditablePanel" )
