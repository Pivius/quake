
--[[---------------------------------------------------------

  Sandbox Gamemode

  This is GMod's default gamemode

-----------------------------------------------------------]]

include( 'shared.lua' )
include( 'getset.lua')
include( 'UtiliT.lua' )

include('cl_fonts.lua')

include( 'sh_editor.lua' )
include( 'cl_editor.lua' )
include( 'sh_movement.lua' )
include( 'sh_jumps.lua' )
include( 'cl_armor.lua' )
include( 'cl_hud.lua' )
include( 'cl_sonar.lua')
include('cl_hitpoints.lua')
--include( 'pedit.lua')
--
-- Make BaseClass available
--
DEFINE_BASECLASS( "gamemode_base" )

local files = file.Find( "quake/gamemode/admin/cmds/*.lua", "LUA" )
for _,v in pairs( files ) do
	include( "quake/gamemode/admin/cmds/" .. v )
end

hook.Add( "PlayerFootstep", "PlayerModelFootstep", function( ply, pos, foot, sound, volume, filter )
		return true
end )


function GM:Initialize()
	BaseClass.Initialize( self )

end
	InitalizeFonts()
