
Trails = {} or Trails

local meta = FindMetaTable( "Player" )
meta.Trail = {}

function meta:TrailColor(col)
  if not col then self.Trail.Color = Color(255, 255, 255, 255) return end
  self.Trail.Color = col
end

function meta:TrailMat(mat)
  if not mat then self.Trail.Material = "trails/laser.vmt" return end
  self.Trail.Material = mat
end

function meta:TrailAdditive(add)
  if not add then self.Trail.Additive = false return end
  self.Trail.Additive = add
end

function meta:TrailLife(int)
  if not int then self.Trail.Lifetime = 1 return end
  self.Trail.Lifetime = int
end

function meta:TrailSize(startw, endw)
  if not startw or not endw then self.Trail.StartWidth = 2 self.Trail.EndWidth = 1 return end
  self.Trail.StartWidth = startw
  self.Trail.EndWidth = endw
end

function meta:RemoveTrail()
  if !IsValid(self.CurTrail) then return end
  self.CurTrail:Remove()
end

function meta:TrailRes(res)
  if not res then self.Trail.Resolution = 1/(16)*0.5 return end
  self.Trail.Resolution = res
end

function meta:CreateTrail()
  self.CurTrail = util.SpriteTrail(self, 0, self.Trail.Color, self.Trail.Additive, self.Trail.StartWidth, self.Trail.EndWidth, self.Trail.Lifetime, self.Trail.Resolution, self.Trail.Material)
end
