include("my_sql.lua")
SQL = {}
SQLdata = SQLdata or {}
SQL.Use = true -- Set this to false if you don't want MySQL

local data = arena.database

mysql:Disconnect()
if SQL.Use then
	mysql:Connect(data.Host, data.User, data.Pass, data.Database, data.Port)
end



hook.Add("DatabaseConnected", "Create Tables", function()
    local qarmor = mysql:Create(game.GetMap() .. "_" .. SQLDetails.tables.armor)
      qarmor:Create("id", "INTEGER NOT NULL AUTO_INCREMENT")
      qarmor:Create("pos", "VARCHAR(255) NOT NULL")
      qarmor:Create("type", "TINYINT(255)")
      qarmor:Create("cooldown", "TINYINT(255)")
      qarmor:PrimaryKey("id")
    qarmor:Execute()
    local qhealth = mysql:Create(game.GetMap() .. "_" .. SQLDetails.tables.health)
      qhealth:Create("id", "INTEGER NOT NULL AUTO_INCREMENT")
      qhealth:Create("pos", "VARCHAR(255) NOT NULL")
      qhealth:Create("type", "TINYINT(255)")
      qhealth:Create("cooldown", "TINYINT(255)")
      qhealth:PrimaryKey("id")
    qhealth:Execute()
    local qweapons = mysql:Create(game.GetMap() .. "_" .. SQLDetails.tables.weapons)
      qweapons:Create("id", "INTEGER NOT NULL AUTO_INCREMENT")
      qweapons:Create("pos", "VARCHAR(255) NOT NULL")
      qweapons:Create("weapon", "VARCHAR(255)")
      qweapons:Create("model", "VARCHAR(255)")
      qweapons:Create("color", "VARCHAR(255)")
      qweapons:Create("cooldown", "TINYINT(255)")
      qweapons:PrimaryKey("id")
    qweapons:Execute()
    local qadmins = mysql:Create(SQLDetails.tables.users)
			qadmins:Create("sID", "VARCHAR(255)")
      qadmins:Create("name", "VARCHAR(255)")
      qadmins:Create("rank", "VARCHAR(255)")
			qadmins:Create("flags", "VARCHAR(255)")
      qadmins:PrimaryKey("sID")
    qadmins:Execute()
		local qranks = mysql:Create(SQLDetails.tables.ranks)
			qranks:Create("name", "VARCHAR(255)")
			qranks:Create("inheritance", "VARCHAR(255)")
			qranks:Create("cmds", "VARCHAR(255)")
			qranks:Create("power", "TINYINT(255)")
			qranks:PrimaryKey("name")
		qranks:Execute()
		Admins:Init()
end)

timer.Create( "mysql.Timer", 1, 0, function()
    mysql:Think()
end )
