arena = {}
SQLDetails = {
  Host = "127.0.0.1", User = "root",
  Pass = "", Database = "gmod_arena",
  Port = 3306
}


local tables = {}

tables.bans  = "arena_bans"
tables.armor = "arena_armor"
tables.health = "arena_health"
tables.weapons = "arena_weapons"
tables.cmds  = "arena_cmds"
tables.users = "arena_users"
tables.ranks = "arena_ranks"


SQLDetails.tables = tables
arena.database = SQLDetails
