hitPoints = {} or hitPoints
hitPoints.Hits = {}

--VARIABLES
local lifetime = 5
local velo = Vector(0,0,0.5)

--FUNCTIONS
function hitPoints.DrawDmg(dmg, col)
  local w, h = surface.GetTextSize(dmg)
  surface.SetFont("HUD HitPoints")
  surface.SetTextColor( col )
  surface.SetTextPos( -w, -h )
  surface.DrawText( dmg )
end

function getHitColor(wep)
  local ply = LocalPlayer()
  local color = Color(255,255,255,255)
  if !ply:Alive() then return color end

  if wep == "weapon_rocketlauncher" then
    color = Color(255, 0, 0, 255)
  elseif wep == "weapon_ion" then
    color = Color(0, 100, 255, 255)
  elseif wep == "weapon_rail" then
    color = Color(255, 255, 0, 255)
  elseif wep == "weapon_qshotgun" then
    color = Color(100, 255, 100, 255)
  end
  return color
end

function createHitPoints()
  local att      = net.ReadEntity()
  local vic      = net.ReadEntity()
  local damage   = math.Round(net.ReadFloat(), 0)
  local position = net.ReadVector()
  local wep      = net.ReadEntity():GetClass()

  local hitdata  = {
    attacker = att,
  victim = vic,
  dmg = damage,
  pos = Vector(math.random(-7,7),math.random(-7,7),math.random(-7,7)) +position,
  vel = velo,
  col = getHitColor(wep),
  life = lifetime

}
  table.insert( hitPoints.Hits, hitdata )

end
net.Receive("Send dmginfo", createHitPoints)

function lifeTime()
  local hits = #hitPoints.Hits
	if hits == 0 then return end

	-- Update hit texts.
	for k, v in pairs(hitPoints.Hits) do
    v.pos = UT:SmoothVector(v.life/10, v.pos, v.pos+v.vel)
    v.col.a = UT:Lerp(v.life/10, v.col.a, v.col.a-v.life)
	end

  for k, v in pairs(hitPoints.Hits) do
    if v.col.a <= 0 then
      table.remove(hitPoints.Hits, k)
    end
  end

end
hook.Add( "Tick", "hitPoints lifetime", lifeTime)


function hitPoints.Draw()
  local ply = LocalPlayer()
  local view = ply:GetViewEntity()
	local ang = view:EyeAngles()
	ang:RotateAroundAxis( ang:Forward(), 90 )
	ang:RotateAroundAxis( ang:Right(), 90 )
	ang = Angle( 0, ang.y, ang.r )


    for k, v in pairs(hitPoints.Hits) do
      cam.Start3D2D( v.pos, ang, v.pos:Distance(ply:EyePos())/520)
        cam.IgnoreZ(true)
          hitPoints.DrawDmg(v.dmg, v.col)

        cam.IgnoreZ(false)
      cam.End3D2D()
    end

end
hook.Add("PostDrawTranslucentRenderables", "drawHitpoints", hitPoints.Draw)
