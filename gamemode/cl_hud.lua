-------------------------------------
---------STENCIL BUFFERS INDICATOR
-------------------------------------
-- UI.
-------------------------------------
hud = hud or {}
hud.size = 0.05/2
hud.distance = 11/2
-------------------------------------
-- Commands.
-- Different console commands for the UI.
-------------------------------------
local hudtoggle   = CreateClientConVar("pkr_hud", "1", true, true, "Hides/shows hud")
-------------------------------------
-- The main UI colors.
-------------------------------------
hud.color = Color(0, 0, 0, 150)
hud.select = 0
-------------------------------------
-- Removes standard UI.
-------------------------------------
function GM:HUDDrawTargetID()
end
function GM:HUDShouldDraw(name)
    local draw = true
    if( name == "CHudHealth" or name == "CHudDamageIndicator" or name == "CHudBattery" or name == "CHudAmmo" or name == "CHudSecondaryAmmo" or name == "CHudCrosshair" or name == "CHudWeaponSelection") then
    draw = false;
    end
return draw;
end

function SelectWep(wep, cmd)
  hud.select = wep
  if wep == 1 then
    wep = "WEP1"
  elseif wep == 2 then
    wep = "weapon_rocketlauncher"
  elseif wep == 3 then
    wep = "WEP3 GRENADE LAUNCHER"
  elseif wep == 4 then
    wep = "weapon_qshotgun"
  elseif wep == 5 then
    wep = "weapon_ion"
  elseif wep == 6 then
    wep = "WEP6 PLASMA"
  elseif wep == 7 then
    wep = "weapon_rail"
  end
  for k,v in pairs (LocalPlayer():GetWeapons()) do
    if v:GetClass() == wep then
      cmd:SelectWeapon(v)
      return v:GetClass() == wep
    end
  end
end



function WeaponSelect( cmd )
  /*if cmd:GetMouseWheel() > 0 then
    cmd:SetMouseWheel(0)
    --print("ax")
    if hud.select > 8 then
      hud.select = 1
    elseif hud.select < 1 then
      hud.select = 7
    end
    SelectWep(hud.select + 1, cmd)
    print(hud.select .. "\n")
  elseif cmd:GetMouseWheel() < 0 then
    if hud.select > 7 then
      hud.select = 1
    elseif hud.select < 1 then
      hud.select = 7
    end
    SelectWep(hud.select-1, cmd)
  end
*/
  if input.IsKeyDown( KEY_2 ) then
    SelectWep(2, cmd)
  elseif input.IsKeyDown( KEY_4 ) then
    SelectWep(4, cmd)
  elseif input.IsKeyDown( KEY_5 ) then
    SelectWep(5, cmd)
  elseif input.IsKeyDown( KEY_7 ) then
    SelectWep(7, cmd)
  end
end
hook.Add( "CreateMove", "WeaponSwitch", WeaponSelect)
-------------------------------------
-- Crosshair UI.
-------------------------------------
function hud.crosshair()
    local SCREEN_W       = ScrW();
    local SCREEN_H       = ScrH();
    local X_MULTIPLIER   = ScrW() / SCREEN_W;
    local Y_MULTIPLIER   = ScrH() / SCREEN_H;
    local ply      = LocalPlayer()


    surface.SetFont("HUD cross")
    surface.SetTextColor( 255,255,255,255 )
    surface.SetTextPos( ScrW( )/2.034, ScrH()/2.12 )
    surface.DrawText( "+" )
end



function hud.Health(  )

  local       SCREEN_W = ScrW();
  local       SCREEN_H = ScrH();
  local   X_MULTIPLIER = ScrW() / SCREEN_W;
  local   Y_MULTIPLIER = ScrH() / SCREEN_H;
  local            ply = LocalPlayer()
  local             HP = ply:Health()
  -------------------------------------
  -- UI Background.
  -------------------------------------
  surface.SetDrawColor(hud.color)
  surface.DrawRect( X_MULTIPLIER*100, Y_MULTIPLIER*600,  2.5 * ply:GetMaxHealth(), 10)
  -------------------------------------
  -- Health bar delay
  -------------------------------------
  Health = Health and Lerp(0.05, Health, HP) or HP

  MegaHealth = MegaHealth and Lerp(0.05, MegaHealth, HP-100) or HP-100

  surface.SetDrawColor(Color(255, 75, 75, 255))
  surface.DrawRect(X_MULTIPLIER*351-(2.5* math.Clamp(Health, 0, 100)), Y_MULTIPLIER*600, 2.5* math.Clamp(Health, 0, 100), 10)

    surface.SetDrawColor(Color(75, 75, 255, 255))
    surface.DrawRect(X_MULTIPLIER*352-(2.5*math.Clamp(MegaHealth, 0, 100)), Y_MULTIPLIER*600, 2.5*math.Clamp(MegaHealth, 0, 200), 10)
  surface.SetTextColor(Color(255, 255, 255, 255))
  surface.SetFont( "HUD Health" )
  surface.SetTextPos( X_MULTIPLIER*351-((2.5* math.Clamp(Health, 8, 100))), Y_MULTIPLIER*600 )
  if HP < 0 then
    surface.DrawText( 0 )
  else
    surface.DrawText( HP )
  end
end


function hud.Armor(  )

  local       SCREEN_W = ScrW();
  local       SCREEN_H = ScrH();
  local   X_MULTIPLIER = ScrW() / SCREEN_W;
  local   Y_MULTIPLIER = ScrH() / SCREEN_H;
  local            ply = LocalPlayer()
  local          Armor = ply:GetArmour()
  local      ArmorType = ply:GetArmorType()
  -------------------------------------
  -- UI Background.
  -------------------------------------
  surface.SetDrawColor(hud.color)
  surface.DrawRect( X_MULTIPLIER*930, Y_MULTIPLIER*600,  2.5 * ply:GetMaxHealth(), 10)
  -------------------------------------
  -- Health bar delay
  -------------------------------------
  Armour = Armour and Lerp(0.05, Armour, Armor) or Armor
  if ArmorType == "Green" then
    surface.SetDrawColor(Color(75, 255, 75, 255))
  elseif ArmorType == "Yellow" then
    surface.SetDrawColor(Color(255, 255, 75, 255))
  elseif ArmorType == "Red" then
    surface.SetDrawColor(Color(255, 15, 15, 255))
  end
  surface.DrawRect(X_MULTIPLIER*930, Y_MULTIPLIER*600, 2.5* math.Clamp(Armour, 0, 100), 10)

  surface.SetTextColor(Color(255, 255, 255, 255))
  surface.SetFont( "HUD Health" )
  surface.SetTextPos( X_MULTIPLIER*880+((2.5* math.Clamp(Armour, 20, 100))), Y_MULTIPLIER*600 )
  if Armor < 0 then
    surface.DrawText( 0 )
  else
    surface.DrawText( math.Round(Armor) )
  end
end


function hud.Speed(  )

  local       SCREEN_W = ScrW();
  local       SCREEN_H = ScrH();
  local   X_MULTIPLIER = ScrW() / SCREEN_W;
  local   Y_MULTIPLIER = ScrH() / SCREEN_H;
  local            ply = LocalPlayer()
  local          Speed = ply:GetVelocity()
  Speed = Vector(Speed.x, Speed.y, 0):Length()


  surface.SetTextColor(Color(255, 255, 255, 255))
  surface.SetFont( "HUD Health" )
  surface.SetTextPos( X_MULTIPLIER*(SCREEN_W/1.82-surface.GetTextSize( math.Round(Speed) .. " UPS" )), Y_MULTIPLIER*10 )
  surface.DrawText( math.Round(Speed) .. " UPS" ) --Speed/1.4

end


function hud.Ammo(  )

  local       SCREEN_W = ScrW()
  local       SCREEN_H = ScrH()
  local   X_MULTIPLIER = ScrW() / SCREEN_W;
  local   Y_MULTIPLIER = ScrH() / SCREEN_H;
  local            ply = LocalPlayer()
  local WeaponSlct = Material("materials/quake/weapon icons/Weapon select.png", "smooth")

  local function GetAmmo( pl, wep )
	if ( !IsValid( pl ) ) then return 0 end
	if ( !IsValid( wep ) ) then return 0 end

	return wep:Clip1()
  end
  local function QuickColor(restcol, activecol, weapon, pl)
    if pl:HasWeapon(weapon) and IsValid(pl:GetActiveWeapon()) then
      if pl:GetActiveWeapon():GetClass() == weapon then
        surface.SetDrawColor(activecol)
      else
        surface.SetDrawColor(restcol)
      end
    else
      surface.SetDrawColor(restcol)
    end
  end
--https://www.youtube.com/watch?v=Pe0KvQQDPRE
  --Weapon Select Bg
  for i=1, 7 do
    surface.SetMaterial(WeaponSlct)
    if i == 2 then
      QuickColor(Color(10, 10, 10, 100), Color(255, 0, 0, 255), "weapon_rocketlauncher", ply)
    elseif i == 5 then
      QuickColor(Color(10, 10, 10, 100), Color(0, 100, 255, 100), "weapon_ion", ply)
    elseif i == 7 then
      QuickColor(Color(10, 10, 10, 100), Color(255, 255, 0, 100), "weapon_rail", ply)
    elseif i == 4 then
      QuickColor(Color(10, 10, 10, 100), Color(100, 255, 100, 100), "weapon_qshotgun", ply)
    else
      surface.SetDrawColor(Color(10, 10, 10, 100))
    end

    surface.DrawTexturedRect((SCREEN_W/3.5)+(i*60), SCREEN_H/1.17, 55, 55)
  end

    --Machine Gun
    local MachineGun = Material("materials/quake/weapon icons/Machine Gun.png", "smooth")
    local MachinePosX, MachinePosY = (SCREEN_W/3.5)+62, SCREEN_H/1.165
    --Ammo
    surface.SetTextColor(Color(255, 255, 255, 255))
    surface.SetFont( "HUD Ammo" )
    surface.SetTextPos( ((MachinePosX*1.04) ), SCREEN_H/1.08)
    surface.DrawText( 0 )
    --Lightning Gun Icon
    surface.SetMaterial(MachineGun)
    surface.SetDrawColor(Color(255, 255, 0, 255))
    surface.DrawTexturedRect(MachinePosX, MachinePosY, 50, 50)

  --Rocket Launcher
  local RocketLauncher = Material("materials/quake/weapon icons/Rocket Launcher.png", "smooth")
  local RocketPosX, RocketPosY = (SCREEN_W/3.5)+61*2, SCREEN_H/1.165
  --Ammo
  surface.SetTextColor(Color(255, 255, 255, 255))
  surface.SetFont( "HUD Ammo" )
  surface.SetTextPos( RocketPosX*1.02, SCREEN_H/1.08)
  surface.DrawText( GetAmmo( ply, ply:GetWeapon( "weapon_rocketlauncher" ) ) )
  --Rocket Launcher Icon
  surface.SetMaterial(RocketLauncher)
  QuickColor(Color(255, 0, 0, 255), Color(255, 255, 255, 255), "weapon_rocketlauncher", ply)
  surface.DrawTexturedRect(RocketPosX, RocketPosY, 50, 50)

  --Grenade Launcher
  local GrenadeLauncher = Material("materials/quake/weapon icons/Grenade Launcher.png", "smooth")
  local NadePosX, NadePosY = (SCREEN_W/3.5)+61*3, SCREEN_H/1.165
  --Ammo
  surface.SetTextColor(Color(255, 255, 255, 255))
  surface.SetFont( "HUD Ammo" )
  surface.SetTextPos( ((NadePosX*1.03) ), SCREEN_H/1.08)
  surface.DrawText( 0 )
  --Grenade Launcher Icon
  surface.SetMaterial(GrenadeLauncher)
  surface.SetDrawColor(Color(0, 155, 0, 255))
  surface.DrawTexturedRect(NadePosX, NadePosY, 50, 50)

  --Shotgun
  local Shotgun = Material("materials/quake/weapon icons/Shotgun.png", "smooth")
  local ShotgunPosX, ShotgunPosY = (SCREEN_W/3.5)+60.5*4, SCREEN_H/1.165
  --Ammo
  surface.SetTextColor(Color(255, 255, 255, 255))
  surface.SetFont( "HUD Ammo" )
  surface.SetTextPos( (ShotgunPosX*1.01 ), SCREEN_H/1.08)
  surface.DrawText( GetAmmo( ply, ply:GetWeapon( "weapon_qshotgun" ) ) )
  --Shotgun Icon
  surface.SetMaterial(Shotgun)
  surface.SetDrawColor(Color(255, 155, 0, 255))
  surface.DrawTexturedRect(ShotgunPosX, ShotgunPosY, 50, 50)

  --Lightning Gun
  local LightningGun = Material("materials/quake/weapon icons/Lightning Gun.png", "smooth")
  local LightningPosX, LightningPosY = (SCREEN_W/3.5)+61*5, SCREEN_H/1.165
  --Ammo
  surface.SetTextColor(Color(255, 255, 255, 255))
  surface.SetFont( "HUD Ammo" )
  surface.SetTextPos( ((LightningPosX*1.01) ), SCREEN_H/1.08)
  surface.DrawText( GetAmmo( ply, ply:GetWeapon( "weapon_ion" ) ) )
  --Lightning Gun Icon
  surface.SetMaterial(LightningGun)
  QuickColor(Color(0, 100, 255, 255), Color(255, 255, 255, 255), "weapon_ion", ply)
  surface.DrawTexturedRect(LightningPosX, LightningPosY, 50, 50)

  --Plasma Gun
  local PlasmaGun = Material("materials/quake/weapon icons/Plasma Gun.png", "smooth")
  local PlasmaPosX, PlasmaPosY = (SCREEN_W/3.5)+60.5*6, SCREEN_H/1.164
  --Ammo
  surface.SetTextColor(Color(255, 255, 255, 255))
  surface.SetFont( "HUD Ammo" )
  surface.SetTextPos( ((PlasmaPosX*1.03) ), SCREEN_H/1.08)
  surface.DrawText( 0 )
  --Plasma Gun Icon
  surface.SetMaterial(PlasmaGun)
  surface.SetDrawColor(Color(100, 0, 255, 255))
  surface.DrawTexturedRect(PlasmaPosX, PlasmaPosY, 50, 50)

  --Rail gun
  local RailGun = Material("materials/quake/weapon icons/Rail gun.png", "smooth")
  local RailPosX, RailPosY = (SCREEN_W/3.5)+60.5*7, SCREEN_H/1.164
  --Ammo
  surface.SetTextColor(Color(255, 255, 255, 255))
  surface.SetFont( "HUD Ammo" )
  surface.SetTextPos( ((RailPosX*1.02) ), SCREEN_H/1.08)
  surface.DrawText( GetAmmo( ply, ply:GetWeapon( "weapon_rail" ) ) )
  --Rail Gun Icon
  surface.SetMaterial(RailGun)

  QuickColor(Color(255, 255, 0, 255), Color(255, 255, 255, 255), "weapon_rail", ply)
  surface.DrawTexturedRect(RailPosX, RailPosY, 50, 50)


end


function hud.Draw()
  hud.crosshair()
  if !LocalPlayer():GetNoclip() then
    hud.Health()
    hud.Armor()
    hud.Speed()
    hud.Ammo()
  end
end
hook.Add("HUDPaint", "DrawHUD", hud.Draw)
