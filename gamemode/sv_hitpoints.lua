util.AddNetworkString( "Send dmginfo" )
function TakeDamage(ent, info)
  if !ent:IsPlayer() or ent == info:GetAttacker() or !ent:Alive() or info:GetDamage() == 0 then return end
  local attacker = info:GetAttacker()
  local victim   = ent
  local damage   = info:GetDamage()
  local pos      = ent:GetPos()+ Vector(0,0,50)
  local wep      = info:GetInflictor()
  if attacker:GetEyeTrace().Entity == victim then
    pos = attacker:GetEyeTrace().HitPos
  end
  net.Start("Send dmginfo")
    net.WriteEntity(attacker)
    net.WriteEntity(victim)
    net.WriteFloat(damage)
    net.WriteVector(pos)
    net.WriteEntity(wep)
  net.Send(attacker)
end
hook.Add("EntityTakeDamage", "Register when taking dmg", TakeDamage)
