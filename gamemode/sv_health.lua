local meta = FindMetaTable( "Player" )
local mega = {}
local savedHP = {}
local storedHP = {}
function meta:SetMega(bool)
  mega[ self ] = bool
  if bool == true then
    savedHP[ self ] = self:Health()+10000
    storedHP[ self ] = self:Health()+10000
  elseif bool == false then
    savedHP[ self ] = 0
    storedHP[ self ] = 0
  end
end

function meta:GetMega()
  if !mega[ self ] then
    return false
  end
  return mega[ self ]
end

function meta:MegaHPloss(loss)
  if !savedHP[self] then return end
  savedHP[ self ] = savedHP[ self ] - loss
end

function meta:GetMegaHPloss()
  if !savedHP[self] then return end
  return savedHP[ self ]
end

function meta:GetMegaHPstored()
  if !storedHP[self] then return end
  return storedHP[ self ]
end

function MegaHealth( ent, info )

	if ent:IsPlayer() then
    local hp = ent:Health()
    local prevhp = savedHP[self]
    local armor = ent:GetArmour()
    local absorb = ent:GetAbsorbtion()
    --if !info:IsFallDamage() then
      if armor >= 1 then
        if ent:GetMega() then
          ent:MegaHPloss(info:GetDamage()*(1-absorb))
        end
      else
        if ent:GetMega() then
          ent:MegaHPloss(info:GetDamage())
        end
      end
  end
end
hook.Add("EntityTakeDamage", "MegaHealth", MegaHealth)


Lastthink = 0
function Think()
  if CurTime() > (Lastthink or 0) + 1 then
    Lastthink = CurTime()
    for _,ply in pairs (player.GetAll()) do
      local hp = ply:Health()
      local Mega = ply:GetMega()
      if ply:GetMegaHPstored() then
        if ply:GetMegaHPloss() < ply:GetMegaHPstored() - 100 then
          ply:SetMega(false)
          print("megaoff")
        end
      end
      if hp/100 > 100/100  then
        if (ply.LastHit or 0) + 5 < CurTime() then
          ply:SetHealth (hp-1)
          ply:MegaHPloss(1)
        end
      end
    end
  end
end
hook.Add("Think", "MegaHPlose", Think)
