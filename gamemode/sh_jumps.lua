local meta = FindMetaTable( "Player" )
jumps = {}
--AccessorFunc( meta, "hop", "CanHop", FORCE_BOOL )
--AccessorFunc( meta, "jumps", "PlayerJumps", FORCE_NUMBER )



function jumps:Init()
	GetSet("CanHop", "Player")
	GetSet("Hops", "Player")
	GetSet("TimerHop", "Player")
	--removeGetSet("canHop", "Player")
	--PrintTable(getmetatable(meta))

end
jumps:Init()


function DetectLedge(ply, cmd, cud)
	local trace = {}
		trace.start = ply:GetPos()
		trace.endpos = (trace.start - Vector(0,0,2) + Vector(-ply:GetAimVector():Angle():Right().y, ply:GetAimVector():Angle():Right().x,0)*35 )

		local trLow = util.TraceHull( trace )

		local trace2 = {}
		trace2.start = ply:GetPos()
		trace2.endpos = (trace2.start + Vector(0,0,15) + Vector(-ply:GetAimVector():Angle():Right().y, ply:GetAimVector():Angle():Right().x,0)*35 )


			local trHigh = util.TraceHull( trace2 )
			if (!trHigh.HitWorld and trLow.HitWorld) and !ply:IsOnGround() then
				cmd:SetVelocity(cmd:GetVelocity() + Vector(-ply:GetAimVector():Angle():Right().y, ply:GetAimVector():Angle():Right().x,2)*200)

			end
end
--hook.Add("SetupMove", "Detect Ledge", DetectLedge)

function meta:IsJumping()
	local onground = self:IsOnGround()
	local jumpkey  = self:KeyDown(IN_JUMP)
	local jumping  = false
	/*
	local trace 	 = {}
	trace.start 	 = self:GetPos()
	trace.endpos 	 = (trace.start - Vector(0,0,2)  )
	local tr 			 = util.TraceHull( trace )
	--tr.HitWorld
	*/

	if onground and jumpkey then
		if IsFirstTimePredicted() then
			return true
		end
	elseif onground then
		return false
	end
end

function SlideRamps(ply, cmd)
	local Pos 			= ply:GetPos()
  local Mins 		= ply:OBBMins()
  local Maxs 		= ply:OBBMaxs()
  local endPos 	= Pos * 1
	local velocity  = cmd:GetVelocity()
  local curspeed 	= Vector(velocity.x, velocity.y, 0)
	local ang = cmd:GetMoveAngles()
	local pos = cmd:GetOrigin()
	local original_velocity = UT.math:vectorCopy(velocity, original_velocity)
	local primal_velocity   = UT.math:vectorCopy(velocity, primal_velocity);
	local endpos = Vector()

  endPos.z = endPos.z -1

  local tr = util.TraceHull{
    start = Pos,
    endpos = endPos,
    mins = Mins,
    maxs = Maxs,
    mask = MASK_PLAYERSOLID_BRUSHONLY,
    filter = function(e1, e2)
      return not e1:IsPlayer()
    end
  }

  if(tr.Fraction ~= 1) then
  -- Gets the normal vector of the surface under the player
		local Normal = tr.HitNormal
  -- Make sure it's not flat ground and not a surf ramp (1.0 = flat ground, < 0.7 = surf ramp)
		UT.math:vectorMA( cmd:GetOrigin(), FrameTime(), velocity, endpos );
		//Don't decrease speed up or down slopes
		--velocity.z = velocity.z - (GetConVar("sv_gravity"):GetFloat() * engine.TickInterval() * 0.5);

		ClipVelocity(cmd:GetVelocity(), Normal, velocity, 1)

		velocity:Normalize()
		velocity.x = velocity.x * cmd:GetVelocity():Length()
		velocity.y = velocity.y * cmd:GetVelocity():Length()
		velocity.z = velocity.z * cmd:GetVelocity():Length() * 1.4

		if ply:IsJumping() and curspeed:Length() > 500 then
			if velocity.z > 0 then
				--dply:SetplyFriction(0)
				cmd:SetVelocity(velocity) -- + Vector(0,0,120)
			end
		end

  end
end
hook.Add("SetupMove", "SlideRamps", SlideRamps)

function meta:DoubleJump()
	if not IsFirstTimePredicted() then return end

	if !self:GetHops() then
		self:SetHops(1)
	end
	local jumps = self:GetHops()
	local mul = (250 * 2)+10
	if jumps == 1 then
		self:SetTimerHop(CurTime() + 0.4)
	elseif jumps > 1 then
		self:SetTimerHop(CurTime() + 0.66)
	end
		self:SetHops( self:GetHops() + 1)

	if jumps >= 2  then
		self:SetJumpPower(mul)

  else
		self:SetJumpPower(300)
	end
end

function Bhop(ply,cmd)
	--local ply = LocalPlayer()
	if !IsFirstTimePredicted() then return end
	if !ply:Alive() then return end
	if ply:GetCanHop() == nil then
		ply:SetCanHop(true)
	end
	if !ply:GetTimerHop() then
		ply:SetTimerHop(0)
	end
	--items/medshot4.wav
	--physics/plaster/ceiling_tile_impact_hard2.wav
	--plats/hall_elev_door.wav
	local sound = "plats/hall_elev_door.wav"
	if !ply:IsOnGround() and ply:GetCanHop() then

  	cmd:SetButtons( bit.band( cmd:GetButtons(), bit.bnot( IN_JUMP ) ) )
  end
	if ply:KeyReleased(IN_JUMP) then
		ply:SetCanHop(true)
	end
	if CurTime() >= ply:GetTimerHop() then

		ply:SetHops(1)
		ply:SetJumpPower(300)
	end
	--print
	if ply:IsOnGround() and bit.band( cmd:GetButtons(), IN_JUMP ) ~= 0 and bit.band( cmd:GetOldButtons(), IN_JUMP ) == 0 and ply:GetCanHop() then
		ply:SetCanHop(false)
    if bit.band( cmd:GetButtons(), bit.bor(IN_JUMP) ) == bit.bor(2) then
			if SERVER then
				if UT:OnGround(ply) then
					ply:EmitSound( sound, 150, 100,1, CHAN_STATIC)
				end
			end
      ply:DoubleJump()
    end
	end
end
hook.Add( 'SetupMove', 'Queue Hop', Bhop)
