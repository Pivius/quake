local meta = FindMetaTable( "Player" )
local Armor = {}
local Absorb = {}
local ArmorType = {}

function meta:SetArmour(val)
  if val < 0 then Armor[ self ] = 0 end
  Armor[ self ] = val

end

function meta:GetArmour()
  if !Armor[ self ] then
    return 0
  end
  return Armor[ self ]
end

function meta:SetAbsorbtion(fraction)
  Absorb[ self ] = fraction

end

function meta:GetAbsorbtion()
  if !Absorb[ self ] then
    return 0
  end
  return Absorb[ self ]
end

function meta:SetArmorType(tp)
  ArmorType[ self ] = tp
end

function meta:GetArmorType()
  if !ArmorType[ self ] then
    return "None"
  end
  return ArmorType[ self ]
end

net.Receive("Update Armor", function(len)
  value = net.ReadFloat()
  ply = net.ReadEntity()
  ply:SetArmour(value)
  print("test")
end)

net.Receive("Update Absorb", function(len)
  value = net.ReadFloat()
  ply = net.ReadEntity()
  ply:SetAbsorbtion(value)
end)

net.Receive("Update Armor Type", function(len)
  tp = net.ReadString()
  ply = net.ReadEntity()
  ply:SetArmorType(tp)
end)
