--
--  ___  ___   _   _   _    __   _   ___ ___ __ __
-- |_ _|| __| / \ | \_/ |  / _| / \ | o \ o \\ V /
--  | | | _| | o || \_/ | ( |_n| o ||   /   / \ / 
--  |_| |___||_n_||_| |_|  \__/|_n_||_|\\_|\\ |_|  2009
--										 
--

list.Set( "RopeMaterials", "#ropematerial.rope",		"cable/rope" )
list.Set( "RopeMaterials", "#ropematerial.cable",		"cable/cable2" )
list.Set( "RopeMaterials", "#ropematerial.xbeam",		"cable/xbeam" )
list.Set( "RopeMaterials", "#ropematerial.laser",		"cable/redlaser" )
list.Set( "RopeMaterials", "#ropematerial.electric",	"cable/blue_elec" )
list.Set( "RopeMaterials", "#ropematerial.physbeam",	"cable/physbeam" )
list.Set( "RopeMaterials", "#ropematerial.hydra",		"cable/hydra" )

local PANEL = {}




vgui.Register( "RopeMaterial", PANEL, "MatSelect" )