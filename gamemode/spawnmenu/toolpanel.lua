
include( 'controlpanel.lua' )

local PANEL = {}

AccessorFunc( PANEL, "m_TabID", "TabID" )

--[[---------------------------------------------------------
	Name: Paint
-----------------------------------------------------------]]
function PANEL:Init()

	
	self.Content = vgui.Create( "DCategoryList", self )
	self.Content:Dock( FILL )
	self.Content:DockMargin( 6, 0, 0, 0 )
	
	self:SetWide( 390 )

	if ( ScrW() > 1280 ) then
		self:SetWide( 460 )
	end
	
end



function PANEL:SetActive( cp )

	local kids = self.Content:GetCanvas():GetChildren()
	for k, v in pairs( kids ) do
		v:SetVisible( false )
	end

	self.Content:AddItem( cp )
	cp:SetVisible( true )
	cp:Dock( TOP )

end

vgui.Register( "ToolPanel", PANEL, "Panel" )
