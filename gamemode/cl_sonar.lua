function drawPlayer(ent)
    local ang = LocalPlayer():EyeAngles()
    local pos = LocalPlayer():EyePos()+ang:Forward()*10

            render.ClearStencil()
            render.SetStencilEnable(true)
            render.SetStencilWriteMask(255)
            render.SetStencilTestMask(255)
            render.SetStencilReferenceValue( 1 )
            render.SetStencilFailOperation(STENCILOPERATION_KEEP)
            render.SetStencilZFailOperation(STENCILOPERATION_REPLACE)
            render.SetStencilPassOperation(STENCILOPERATION_REPLACE)
            render.SetStencilCompareFunction(STENCILCOMPARISONFUNCTION_ALWAYS)
            --[[
            local scale = Vector(1.1,1.03,1.01)
            local mat = Matrix()
            mat:Scale(Vector(scale))
            mat:Translate( Vector(-0.36,0,-0.7) )
            ent:EnableMatrix("RenderMultiply", mat) ]]

                render.SetBlend( 0 )
                    ent:DrawModel()
                render.SetBlend( 1 )

            render.SetStencilReferenceValue( 1 )
            cam.Start3D2D(Vector(pos.x, pos.y, pos.z),Angle(ang.p+90,ang.y,0),1)
                render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
                surface.SetDrawColor(math.cos(RealTime()*2)*255,math.cos(RealTime()*2)*255,math.cos(RealTime()*2)*255,math.cos(RealTime()*2)*150)
                surface.DrawRect(-ScrW(),-ScrH(),ScrW()*2,ScrH()*2)
            cam.End3D2D()



            render.SetStencilEnable(false)

end

function CreateCircle(x, y, startang, endang, radius, thickness, roughness, col, modifier)
  surface.SetDrawColor(col)
  if !modifier then modifier = 1 end
  startang = math.Clamp( startang or 0, 0, 360 );
    endang = math.Clamp( endang or 360, 0, 360 );

    if endang < startang then
        local temp = endang;
        endang = startang;
        startang = temp;
    end

  for i=startang, endang, roughness do
    draw.NoTexture()
    surface.DrawTexturedRectRotated(math.cos( math.rad(i) ) * (radius) + ScrW()/2,math.sin( math.rad(i) ) * (radius) + ScrH()/2, thickness, modifier*2, -i )
  end
end

function AlertSonar()
  local matGlow = Material( "sprites/light_glow02_add" )
  local matBlur = Material("pp/blurscreen")
  local vertices = {}
  local vertices2 = {}
  local radius = math.cos(RealTime())*700
  local mult = 2
  render.ClearStencil()
  render.SetStencilEnable( true )
  	cam.Start2D()

    	render.SetStencilFailOperation( STENCILOPERATION_KEEP )
    	render.SetStencilZFailOperation( STENCILOPERATION_REPLACE )
    	render.SetStencilPassOperation( STENCILOPERATION_REPLACE )
    	render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_ALWAYS )
    	render.SetStencilReferenceValue( 2 )

      for degree=0,360,2 do
        local x1,y1 = math.cos(math.rad(degree)) * radius + ScrW()/2, math.sin(math.rad(degree)) * radius + ScrH()/2
        table.insert(vertices, {x=x1,y=y1})
      end

      surface.SetDrawColor(0,0,0,1)

      draw.NoTexture()
      CreateCircle(ScrW()/2, ScrH()/2,0, 360,
      math.Clamp(-math.cos(RealTime())*1800, 200, 1800)+math.Clamp(math.cos(RealTime())*1800,200, 1800),
      750,
      4, Color(0,0,0,1),40)
      surface.DrawPoly(vertices)

      render.SetStencilCompareFunction( STENCIL_NOTEQUAL )
    	render.SetStencilPassOperation( STENCIL_KEEP )
      render.SetStencilReferenceValue( 2 )
      render.UpdateScreenEffectTexture()
    	render.SetMaterial(matBlur)
    	local steps = 3
    	local multiplier = 10
      surface.SetDrawColor(0,0,0,1)
    	for i = 1, steps do
    		matBlur:SetFloat("$blur", (i / steps)* math.Clamp(math.sin(RealTime()*2)*multiplier, 0, 255))
    		matBlur:Recompute()
    		render.UpdateScreenEffectTexture()
        render.DrawScreenQuad()
  		end
      surface.SetDrawColor(255,255,255,math.Clamp(math.sin(RealTime()*2)*10, 0, 255))
      draw.NoTexture()
      surface.DrawRect(0,0, ScrW(), ScrH())

    cam.End2D()
  render.SetStencilEnable( false )
end
-------------------------------------
-- Render player.
-------------------------------------
afkers = {}
function Drawply(plyr)
    if( ScrW() == ScrH() ) then return end

    if( OUTLINING_ENTITY ) then return end
    OUTLINING_ENTITY = true
    net.Receive("Toggle Sonar", function(len, ply)
      afkers = net.ReadTable()
    end)

    if afkers == {} then OUTLINING_ENTITY = false return end
    for k, v in pairs(afkers) do
      if v == true and k != LocalPlayer() then

        drawPlayer( k )
      end
    end


    OUTLINING_ENTITY = false

end

hook.Add( "PostPlayerDraw", "Test2", Drawply)

hook.Add( "PostDrawOpaqueRenderables",  "PostDrawing14", function()

  net.Receive("Toggle Sonar", function(len, ply)
    afkers = net.ReadTable()
  end)
  for k, v in pairs(afkers) do
    if v == true and k == LocalPlayer() then
      AlertSonar()
    end
  end
end)
