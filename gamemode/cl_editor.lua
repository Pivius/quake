Editor.UI = {} or Editor.UI
Editor.box = {} or Editor.box
Editor.chevron = {} or Editor.chevron

function Editor.box:Create( x, y, w, h, color, text, textx, texty, color2, font)
    surface.SetDrawColor(Color(color.r, color.g, color.b, color.a))
    surface.DrawRect( x, y,  w, h)
    if font then
      surface.SetFont( font )
    else
      surface.SetFont( "Editor Text" )
    end
    surface.SetTextColor(color2)
    surface.SetTextPos( textx, texty )
    surface.DrawText( text )

end

function Editor.chevron:Create( vecx, vecy, size, color)
  local chevron = {
    { x = vecx+(100*size), y = vecy+(0*size) }, -- Top right
    { x = vecx+(150*size), y = vecy+(100*size) }, -- Point
    { x = vecx+(100*size), y = vecy+(200*size) }, -- Bottom right
    { x = vecx+(0*size), y = vecy+(200*size) }, -- Bottom left
    { x = vecx+(100*size), y =vecy+(200*size) }, -- Inner Point
    { x = vecx+(0*size), y = vecy+(0*size) } -- Upper left
  }
  surface.SetDrawColor(Color(color.r, color.g, color.b, color.a))
  draw.NoTexture()
  surface.DrawPoly(chevron)
end

function Editor.UI:Init()

end

Editor.Panels = {}

function Editor.UI:Reset()
  for k ,v in pairs(vgui.GetWorldPanel():GetChildren()) do
    if string.match(tostring(v), "Editor") then
      v:Remove()
    end
  end
end
Editor.UI:Reset()

function Editor.Draw()
  if LocalPlayer():GetNoclip() then
    --Panel
    local Panel = vgui.Create( "Editor panel", nil, "Editor panel" )
    table.insert(Editor.Panels, Panel)
    Panel:SetSize(ScrW()*0.17,ScrH()*0.55)
    Panel:SetCol(Color(0,0,0))
    Panel:SetPos(-50,0)
    Panel:Create()
    Panel:Hide( 0.1, 0, -200, "x")
    timer.Simple(0.1, function() Panel:Show( 150, 0.5, 0, 0, "x") end)

    --Title Panel
    local Title = vgui.Create( "Text", nil, "Editor title" )
    table.insert(Editor.Panels, Title)
    Title:SetSize(ScrW()*0.17,ScrH()*0.55)
    Title:SetCol(Color(0,0,0))
    Title:SetPos(-200,0)
    Title:SetTxt("EDITOR")
    Title:SetParent(Panel.bg)
    Title:Create()
    Title:Hide( 0.1, 0, -200, "x")
    timer.Simple(0.1, function() Title:Show( 255, 0.5, 0, (ScrW()*0.17)/3.1, "x") end)


    local HealthList = vgui.Create( "Scroll", nil, "Editor Health scroll" )
    table.insert(Editor.Panels, HealthList)
    HealthList:SetSize(ScrW()*0.17,ScrH()*0.481)
    HealthList:NewColor(Color(0,0,0))
    HealthList:SetPos(-200,0)
    HealthList:SetTitle("HEALTH")
    HealthList:Create()
    HealthList:Hide( 0.1, 0, -200, "x")

    local ArmorList = vgui.Create( "Scroll", nil, "Editor Armor scroll" )
    table.insert(Editor.Panels, ArmorList)
    ArmorList:SetSize(ScrW()*0.17,ScrH()*0.481)
    ArmorList:NewColor(Color(0,0,0))
    ArmorList:SetPos(-200,0)
    ArmorList:SetTitle("ARMOR")
    ArmorList:Create()
    ArmorList:Hide( 0.1, 0, -200, "x")

    local WeaponsList = vgui.Create( "Scroll", nil, "Editor Weapons scroll" )
    table.insert(Editor.Panels, WeaponsList)
    WeaponsList:SetSize(ScrW()*0.17,ScrH()*0.481)
    WeaponsList:NewColor(Color(0,0,0))
    WeaponsList:SetPos(-200,0)
    WeaponsList:SetTitle("WEAPONS")
    WeaponsList:Create()
    WeaponsList:Hide( 0.1, 0, -200, "x")

    local GreenHP = vgui.Create( "ModelButton", nil, "Editor GreenHP" )
    table.insert(Editor.Panels, GreenHP)
    GreenHP:SetSize(ScrW()*0.1,ScrH()*0.1)
    GreenHP:NewColor(Color(255,255,255))
    GreenHP:ModelCol(Color(0,255,0))
    GreenHP:SetPos(0,0)
    GreenHP:SetDock(TOP)
    GreenHP:SetMargin(0, 25, (ScrW()*0.01), 0)
    GreenHP:SetParent(HealthList.list)
    GreenHP:Create()
    HealthList:Item(GreenHP)


    local YellowHP = vgui.Create( "ModelButton", nil, "Editor YellowHP" )
    table.insert(Editor.Panels, YellowHP)
    YellowHP:SetSize(ScrW()*0.1,ScrH()*0.1)
    YellowHP:NewColor(Color(255,255,255))
    YellowHP:ModelCol(Color(255,255,0))
    YellowHP:SetPos(0,0)
    YellowHP:SetDock(TOP)
    YellowHP:SetMargin(0, 55, (ScrW()*0.01), 0)
    YellowHP:SetParent(HealthList.list)
    YellowHP:Create()
    HealthList:Item(YellowHP)

    local OrangeHP = vgui.Create( "ModelButton", nil, "Editor OrangeHP" )
    table.insert(Editor.Panels, OrangeHP)
    OrangeHP:SetSize(ScrW()*0.1,ScrH()*0.1)
    OrangeHP:NewColor(Color(255,255,255))
    OrangeHP:ModelCol(Color(255,50,0))
    OrangeHP:SetPos(0,0)
    OrangeHP:SetDock(TOP)
    OrangeHP:SetMargin(0, 55, (ScrW()*0.01), 0)
    OrangeHP:SetParent(HealthList.list)
    OrangeHP:Create()
    HealthList:Item(OrangeHP)

    local MegaHP = vgui.Create( "ModelButton", nil, "Editor MegaHP" )
    table.insert(Editor.Panels, MegaHP)
    MegaHP:SetSize(ScrW()*0.1,ScrH()*0.1)
    MegaHP:NewColor(Color(255,255,255))
    MegaHP:ModelCol(Color(0,0,155))
    MegaHP:SetPos(0,0)
    MegaHP:SetDock(TOP)
    MegaHP:SetMargin(0, 55, (ScrW()*0.01), 0)
    MegaHP:SetParent(HealthList.list)
    MegaHP:Create()
    HealthList:Item(MegaHP)
    --HealthList:Show( 150, 0.1, 0, ScrW()*0.171, "x")
    --Title Panel
    local Health = vgui.Create( "Button", nil, "Editor Health" )
    local Armor = vgui.Create( "Button", nil, "Editor Armor" )
    local Weapons = vgui.Create( "Button", nil, "Editor Weapons" )
    table.insert(Editor.Panels, Health)
    Health:SetSize(ScrW()*0.15,ScrH()*0.05)
    Health:SetPos(0,ScrH()*0.05)
    Health:SetTxt("Health")
    Health:NewColor(Color(0,0,0,150))
    Health:NewHighlight(Color(0,0,0,200))
    Health:NewFlash(Color(255,0,0,50))
    Health.Clicked = false
        local function ShowHealth()
          Health.Clicked = true
          Weapons.Clicked = false
          Armor.Clicked = false

          Armor:NewColor(Color(0,0,0,150))
          Armor:NewHighlight(Color(0,0,0,200))
          Armor.panel:ColorTo(Armor.color, 0.5, 0.1)

          Weapons:NewColor(Color(0,0,0,150))
          Weapons:NewHighlight(Color(0,0,0,200))
          Weapons.panel:ColorTo(Weapons.color, 0.5, 0.1)

          Health:NewHighlight(Color(50,0,0,200))
          Health:NewColor(Color(255,0,0,50))
          Health.panel:SetColor(Health.highlight)

          local old = Health.panel:GetColor()
          Health.panel:SetColor(Health.flash)
          Health.panel:ColorTo(old, 0.5, 0.1)
          WeaponsList:Hide( 0.5, 0, -200, "x")
          HealthList:Show( 150, 0.1, 0, ScrW()*0.171, "x")
          ArmorList:Hide( 0.5, 0, -200, "x")
        end

        local function HideHealth()
          Health.Clicked = false
          Health:NewColor(Color(0,0,0,150))
          Health:NewHighlight(Color(0,0,0,200))
          Health.panel:SetColor(Health.highlight)
          local old = Health.panel:GetColor()
          Health.panel:SetColor(Health.flash)
          Health.panel:ColorTo(old, 0.5, 0.1)
          HealthList:Hide( 0.5, 0, -200, "x")
        end
    Health:Funct(function()
      if !Health.Clicked then
        ShowHealth()
      else
        HideHealth()
      end
    end)
    Health:Create()
    Health:Hide( 0.1, 0, -200, "x")
    timer.Simple(0.1, function() Health:Show( 255, 0.5, 0, ScrW()*0.01, "x") end)


    table.insert(Editor.Panels, Armor)
    Armor:SetSize(ScrW()*0.15,ScrH()*0.05)
    Armor:SetPos(-50,ScrH()*0.1)
    Armor:SetTxt("Armor")
    Armor:NewColor(Color(0,0,0,150))
    Armor:NewHighlight(Color(0,0,0,200))
    Armor:NewFlash(Color(0,255,0,50))
    Armor.Clicked = false
    Armor:Funct(function()
      if !Armor.Clicked then
        Armor.Clicked = true
        Health.Clicked = false
        Weapons.Clicked = false
        Health:NewColor(Color(0,0,0,150))
        Health:NewHighlight(Color(0,0,0,200))
        Health.panel:ColorTo(Health.color, 0.5, 0.1)
        Weapons:NewColor(Color(0,0,0,150))
        Weapons:NewHighlight(Color(0,0,0,200))
        Weapons.panel:ColorTo(Weapons.color, 0.5, 0.1)
        Armor:NewHighlight(Color(0,50,0,200))
        Armor:NewColor(Color(0,255,0,50))
        Armor.panel:SetColor(Armor.highlight)
        local old = Armor.panel:GetColor()
        Armor.panel:SetColor(Armor.flash)
        Armor.panel:ColorTo(old, 0.5, 0.1)
        HealthList:Hide( 0.5, 0, -200, "x")
        WeaponsList:Hide( 0.5, 0, -200, "x")
        ArmorList:Show( 150, 0.1, 0, ScrW()*0.171, "x")
      else
        Armor.Clicked = false
        Armor:NewColor(Color(0,0,0,150))
        Armor:NewHighlight(Color(0,0,0,200))
        Armor.panel:SetColor(Armor.highlight)
        local old = Armor.panel:GetColor()
        Armor.panel:SetColor(Armor.flash)
        Armor.panel:ColorTo(old, 0.5, 0.1)
        ArmorList:Hide( 0.5, 0, -200, "x")
      end
    end)
    Armor:Create()
    Armor:Hide( 0.1, 0, -200, "x")
    timer.Simple(0.1, function() Armor:Show( 255, 0.5, 0, ScrW()*0.01, "x") end)

    table.insert(Editor.Panels, Weapons)
    Weapons:SetSize(ScrW()*0.15,ScrH()*0.05)
    Weapons:SetPos(-50,ScrH()*0.15)
    Weapons:SetTxt("Weapons")
    Weapons:NewColor(Color(0,0,0,150))
    Weapons:NewHighlight(Color(0,0,0,200))
    Weapons:NewFlash(Color(0,0,255,50))
    Weapons.Clicked = false
    Weapons:Funct(function()
      if !Weapons.Clicked then
        Weapons.Clicked = true
        Health.Clicked = false
        Armor.Clicked = false
        Health:NewColor(Color(0,0,0,150))
        Health:NewHighlight(Color(0,0,0,200))
        Health.panel:ColorTo(Health.color, 0.5, 0.1)
        Armor:NewColor(Color(0,0,0,150))
        Armor:NewHighlight(Color(0,0,0,200))
        Armor.panel:ColorTo(Armor.color, 0.5, 0.1)
        Weapons:NewHighlight(Color(0,0,50,200))
        Weapons:NewColor(Color(0,0,255,50))
        Weapons.panel:SetColor(Weapons.highlight)
        local old = Weapons.panel:GetColor()
        Weapons.panel:SetColor(Weapons.flash)
        Weapons.panel:ColorTo(old, 0.5, 0.1)
        HealthList:Hide( 0.5, 0, -200, "x")
        ArmorList:Hide( 0.5, 0, -200, "x")
        WeaponsList:Show( 150, 0.1, 0, ScrW()*0.171, "x")
      else
        Weapons.Clicked = false
        Weapons:NewColor(Color(0,0,0,150))
        Weapons:NewHighlight(Color(0,0,0,200))
        Weapons.panel:SetColor(Weapons.highlight)
        local old = Weapons.panel:GetColor()
        Weapons.panel:SetColor(Weapons.flash)
        Weapons.panel:ColorTo(old, 0.5, 0.1)
        WeaponsList:Hide( 0.5, 0, -200, "x")
      end
    end)
    Weapons:Create()
    Weapons:Hide( 0.1, 0, -200, "x")
    timer.Simple(0.1, function() Weapons:Show( 255, 0.5, 0, ScrW()*0.01, "x") end)
  end
end
hook.Add("UpdateDraw", "Draws the Editor UI", Editor.Draw)
Editor.Draw()

function Editor.Remove()
  if LocalPlayer():GetNoclip() then return end
  for k ,v in pairs(Editor.Panels) do
    if v then
      v:Hide( 0.5, 0, -200, "x")
      timer.Create("ERemove " .. tostring(v), 1, 1, function() v:Remove() table.RemoveByValue(Editor.Panels, v) end)

    end
  end
end
hook.Add("Think", "Removes the Editor UI", Editor.Remove)

function GM:OnContextMenuOpen()
  print(LocalPlayer():GetNoclip())
  if !LocalPlayer():GetNoclip() then return end
	gui.EnableScreenClicker(true)

end

function GM:OnContextMenuClose()
  if !LocalPlayer():GetNoclip() then gui.EnableScreenClicker(false) return end
  gui.EnableScreenClicker(false)
end
