function	FlyMove( ply, cmd )
	local			bumpcount, numbumps;
	local		dir;
	local		d;
	local			numplanes;
	local		planes = {};
	local		primal_velocity = Vector();
	local		clipVelocity;
	local			i, j, k;
	local	trace;
	local		endposition = Vector();
	local		time_left;
	local		into;
	local		endVelocity = Vector()
	local		endClipVelocity;
	gravity = GetConVar("sv_gravity"):GetFloat()
	numbumps = 4;
  local EndPos = cmd:GetOrigin() * 1
  EndPos.z = EndPos.z -1
	UT.math:vectorCopy(cmd:GetVelocity(), primal_velocity);
  local trace = util.TraceHull{
    start = cmd:GetOrigin(),
    endpos = EndPos,
    mins = ply:OBBMins(),
    maxs = ply:OBBMaxs(),
    mask = MASK_PLAYERSOLID_BRUSHONLY,
    filter = function(e1, e2)
      return not e1:IsPlayer()
    end
  }

	if ( gravity ) then
		UT.math:vectorCopy( cmd:GetVelocity(), endVelocity );

		endVelocity.z = endVelocity.z-( GetConVar("sv_gravity"):GetFloat() * FrameTime());
    cmd:SetVelocity(( cmd:GetVelocity() + Vector(0,0,endVelocity.z) ) * 0.5)
		primal_velocity.z = endVelocity.z;
		if ( ply:OnGround() ) then
			// slide along the ground plane
      local vel = cmd:GetVelocity()
			ClipVelocity(cmd:GetVelocity(), trace.Normal,
				vel, 1 );
        cmd:SetVelocity(vel)
		end
	end

	time_left = FrameTime();

	// never turn against the ground plane
	if ( ply:OnGround() ) then
		numplanes = 1;
		UT.math:vectorCopy( trace.Normal, planes[0] );
	else
		numplanes = 0;
	end

	// never turn against original velocity
	UT.math:vectorNormalize2( cmd:GetVelocity(), planes[numplanes] );
	numplanes = numplanes +1

	for bumpcount=0, numbumps-1, 1 do

		// calculate position we are trying to move to
		UT.math:vectorMA( cmd:GetOrigin(), time_left, cmd:GetVelocity(), endposition );

		// see if we can make it there
    local trace = util.TraceHull{
      start = cmd:GetOrigin(),
      endpos = endposition,
      mins = ply:OBBMins(),
      maxs = ply:OBBMaxs(),
      mask = MASK_PLAYERSOLID_BRUSHONLY,
      filter = function(e1, e2)
        return not e1:IsPlayer()
      end
    }


		if (trace.AllSolid) then
			// entity is completely trapped in another solid
			cmd:SetVelocity(Vector(0,0,0))	// don't build up falling damage, but allow sideways acceleration
			return true
		end

		if (trace.Fraction > 0) then
			// actually covered some distance
      cmd:SetOrigin(trace.HitPos)
		end

		if (trace.Fraction == 1) then
			 break;		// moved the entire distance
		end

		time_left = time_left - (time_left * trace.Fraction)

		if (numplanes >= 5) then
			// this shouldn't really happen
			cmd:SetVelocity(Vector())
			return true;
		end

		//
		// if this is the same plane we hit before, nudge velocity
		// out along it, which fixes some epsilon issues with
		// non-axial planes
		//
		for  i = 0,  numplanes-1, 1  do
			if ( trace.Normal:Dot(planes[i]) > 0.99 ) then
        cmd:SetVelocity(traceNormal + cmd:GetVelocity())
				break;
			end
		end
		UT.math:vectorCopy(trace.Normal, planes[numplanes]);
		numplanes = numplanes +1

		//
		// modify velocity so it parallels all of the clip planes
		//

		// find a plane that it enters
		for i = 0, numplanes-1, 1 do
			into = cmd:GetVelocity():Dot(planes[i])
			if !( into >= 0.1 ) then
        break;
			end

			// slide along the plane
			ClipVelocity (cmd:GetVelocity(), planes[i], clipVelocity, 1 );

			// slide along the plane
			ClipVelocity (endVelocity, planes[i], endClipVelocity, 1 );

			// see if there is a second plane that the new move enters
			for  j = 0, numplanes-1, 1  do
				if !( j == i ) then
					break;
				end
				if !( clipVelocity:Dot(planes[j])>= 0.1 ) then
					break		// move doesn't interact with the plane
				end

				// try clipping the move to the plane
				ClipVelocity( clipVelocity, planes[j], clipVelocity, 1 );
				ClipVelocity( endClipVelocity, planes[j], endClipVelocity, 1 );

				// see if it goes back into the first clip plane
				if !( clipVelocity:Dot(planes[i])>= 0 ) then
					break;
				end

				// slide the original velocity along the crease
        dir = planes[i]:Cross(planes[j])
				dir:Normalize()
				d = dir:Dot(cmd:GetVelocity());
				UT.math:vectorScale( dir, d, clipVelocity );

				d = dir:Dot(endVelocity)
				UT.math:vectorScale( dir, d, endClipVelocity );

				// see if there is a third plane the the new move enters
				for  k = 0, numplanes-1, 1 do
					if !( k == i || k == j ) then
						break;
					end
					if !( clipVelocity:Dot(planes[k]) >= 0.1 ) then
						break;		// move doesn't interact with the plane
					end

					// stop dead at a tripple plane interaction
					cmd:SetVelocity(Vector())
					return true;
				end
			end

			// if we have fixed all interactions, try another move
      cmd:SetVelocity(clipVelocity)
			UT.math:vectorCopy( endClipVelocity, endVelocity );
			break;
		end
	end



	// don't change velocity if in a timer (FIXME: is this correct?)
  /*
	if ( pm->ps->pm_time ) {
		cmd:SetVelocity(primal_velocity)
	}*/

	return ( bumpcount != 0 );
end
