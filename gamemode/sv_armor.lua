local meta = FindMetaTable( "Player" )
local Armor = {}
local ArmorType = {}
local Absorb = {}
local ArmorEnts = {}

util.AddNetworkString( "Update Armor" )
util.AddNetworkString( "Update Absorb" )
util.AddNetworkString( "Update Armor Type" )

function meta:SetArmour(val)
  net.Start("Update Armor")
    net.WriteFloat(val)
    net.WriteEntity(self)
  net.Send(self)

  if val < 0 then Armor[ self ] = 0 return end
  Armor[ self ] = val
end

function meta:GetArmour()
  if !Armor[ self ] then
    return 0
  end
  return Armor[ self ]
end

function meta:SetAbsorbtion(fraction)
  Absorb[ self ] = fraction
  net.Start("Update Absorb")
    net.WriteFloat(fraction)
    net.WriteEntity(self)
  net.Send(self)
end

function meta:GetAbsorbtion()
  if !Absorb[ self ] then
    return 0
  end
  return Absorb[ self ]
end

function meta:SetArmorType(tp)
  net.Start("Update Armor Type")
    net.WriteString(tp)
    net.WriteEntity(self)
  net.Send(self)

  ArmorType[ self ] = tp
end

function meta:GetArmorType()
  if !ArmorType[ self ] then
    return "None"
  end
  return ArmorType[ self ]
end

function Armour( ent, info )

	if ent:IsPlayer() then

    local armor = ent:GetArmour()
    local absorb = ent:GetAbsorbtion()
    --if !info:IsFallDamage() then
      if armor >= 1 then
        info:ScaleDamage( absorb )
        ent:SetArmour(armor-((info:GetDamage()/absorb)*(1-absorb)))
      end
    --end
  end
end
hook.Add("EntityTakeDamage", "TakeDmg", Armour)

-------------------------------------
-- SQL.
-------------------------------------
-------------------------------------
-- Armor
-------------------------------------
function Armor:Insert(id, position, col, cooldown)
	local pos = util.TypeToString( position )
	local query = mysql:Insert(game.GetMap() .. "_" .. SQLDetails.tables.armor)
  query:Insert("id", id)
  query:Insert("pos", pos)
  query:Insert("type", col)
  query:Insert("cooldown", cooldown)
	query:Execute()
end

function Armor:Update(id, position, col, cooldown)
	local pos = util.TypeToString( position )
	local query = mysql:Update(game.GetMap() .. "_" .. SQLDetails.tables.armor)
		query:Where("id", id)
		query:Update("pos", pos)
		query:Update("type", col)
		query:Update("cooldown", cooldown)
	query:Execute()
end

function Armor:Remove(id)
	local query = mysql:Delete(game.GetMap() .. "_" .. SQLDetails.tables.armor)
		query:Where("id", id)
	query:Execute()
end
-------------------------------------
-- Health
-------------------------------------
Health = {}
function Health:Insert(id, position, col, cooldown)
	local pos = util.TypeToString( position )
	local query = mysql:Insert(game.GetMap() .. "_" .. SQLDetails.tables.health)
  query:Insert("id", id)
  query:Insert("pos", pos)
  query:Insert("type", col)
  query:Insert("cooldown", cooldown)
	query:Execute()
end

function Health:Update(id, position, col, cooldown)
	local pos = util.TypeToString( position )
	local query = mysql:Update(game.GetMap() .. "_" .. SQLDetails.tables.health)
		query:Where("id", id)
		query:Update("pos", pos)
		query:Update("type", col)
		query:Update("cooldown", cooldown)
	query:Execute()
end

function Health:Remove(id)
	local query = mysql:Delete(game.GetMap() .. "_" .. SQLDetails.tables.health)
		query:Where("id", id)
	query:Execute()
end
-------------------------------------
-- Weapons
-------------------------------------
Weapons = {}
function Weapons:Insert(id, position, weapon, model, col, cooldown)
	local pos = util.TypeToString( position )
  local color = util.TypeToString( col )
	local query = mysql:Insert(game.GetMap() .. "_" .. SQLDetails.tables.health)
  query:Insert("id", id)
  query:Insert("pos", pos)
  query:Insert("weapon", weapon)
  query:Insert("model", model)
  query:Insert("color", col)
  query:Insert("cooldown", cooldown)
	query:Execute()
end

function Weapons:Update(id, position, col, cooldown)
	local pos = util.TypeToString( position )
  local color = util.TypeToString( col )
	local query = mysql:Update(game.GetMap() .. "_" .. SQLDetails.tables.health)
		query:Where("id", id)
		query:Update("pos", pos)
    query:Update("weapon", weapon)
    query:Update("model", model)
		query:Update("type", color)
		query:Update("cooldown", cooldown)
	query:Execute()
end

function Weapons:Remove(id)
	local query = mysql:Delete(game.GetMap() .. "_" .. SQLDetails.tables.health)
		query:Where("id", id)
	query:Execute()
end

function LoadItems()

end

--LoadItems()

/*
ent = ents.Create( "armor" )
ent:SetPos( Vector(184, 301, 474) )
ent.Type = "Red"
ent.Cooldown = 5
ent:Spawn()

UT:AddToTable(ArmorEnts, ent)

ent = ents.Create( "armor" )
ent:SetPos( Vector(184, 351, 474) )
ent.Type = "Yellow"
ent.Cooldown = 5
ent:Spawn()

UT:AddToTable(ArmorEnts, ent)

ent = ents.Create( "armor" )
ent:SetPos( Vector(184, 400, 474) )
ent.Type = "Green"
ent.Cooldown = 5
ent:Spawn()

UT:AddToTable(ArmorEnts, ent)

ent = ents.Create( "health" )
ent:SetPos( Vector(184, 250, 474) )
ent.Type = "Green"
ent.Cooldown = 5
ent:Spawn()

UT:AddToTable(ArmorEnts, ent)

ent = ents.Create( "health" )
ent:SetPos( Vector(184, 200, 474) )
ent.Type = "Yellow"
ent.Cooldown = 5
ent:Spawn()

UT:AddToTable(ArmorEnts, ent)

ent = ents.Create( "health" )
ent:SetPos( Vector(184, 150, 474) )
ent.Type = "Orange"
ent.Cooldown = 5
ent:Spawn()

UT:AddToTable(ArmorEnts, ent)

ent = ents.Create( "health" )
ent:SetPos( Vector(184, 100, 474) )
ent.Type = "Blue"
ent.Cooldown = 5
ent:Spawn()

UT:AddToTable(ArmorEnts, ent)

ent = ents.Create( "weapon_spawn" )
ent:SetPos( Vector(300, 400, 474) )
ent.Weapon = "weapon_rocketlauncher"
ent.Model = "models/weapons/w_rocket_launcher.mdl"
ent.Cooldown = 5
ent.color = Color(255,0,0, 255)
ent:Spawn()

UT:AddToTable(ArmorEnts, ent)

ent = ents.Create( "weapon_spawn" )
ent:SetPos( Vector(300, 350, 474) )
ent.Weapon = "weapon_ion"
ent.Model = "models/weapons/w_crossbow.mdl"
ent.Cooldown = 5
ent.color = Color(0,100,255, 255)
ent:Spawn()

UT:AddToTable(ArmorEnts, ent)

ent = ents.Create( "weapon_spawn" )
ent:SetPos( Vector(300, 300, 474) )
ent.Weapon = "weapon_rail"
ent.Model = "models/weapons/w_irifle.mdl"
ent.Cooldown = 5
ent.color = Color(255,255,0, 255)
ent:Spawn()

UT:AddToTable(ArmorEnts, ent)

ent = ents.Create( "weapon_spawn" )
ent:SetPos( Vector(300, 250, 474) )
ent.Weapon = "weapon_qshotgun"
ent.Model = "models/weapons/w_shotgun.mdl"
ent.Cooldown = 5
ent.color = Color(100,255,100, 255)
ent:Spawn()

UT:AddToTable(ArmorEnts, ent)
*/
