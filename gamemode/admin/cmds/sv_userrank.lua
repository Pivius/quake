/*
  NAME - adduser
  FUNCTION - Adds a user to a rank
*/
Admin.Commands:createCMD("adduser", "caller ; user ; rank", function(caller, user, rank)
  if !user or user == "" then
    chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "Please enter a steamID or nick.")
    return
  end

  if !rank or rank == "" then
    chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "Please enter a rank.")
    return
  end

  if !Admin:GetRank(rank) then
    chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "The rank " .. rank .. " does not exist!")
		return
  end

  local nick = Admin.Commands:autoComplete( user )
  if !caller:IsHigherThan( rank ) then
    chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "You can't target this rank!")
    return
  end
  if nick == nil then
    local sid = string.Split( string.lower(user), ":" )
    if string.match(sid[1], "steam_") and isnumber(util.StringToType( sid[2], "Float" )) and isnumber(util.StringToType( sid[2], "Float" )) then
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], 'You added ', Admin.Colors["VGAM"], user, Admin.Colors["normal"], ' to ' .. rank .. ".")
      plyUpdate( user, rank)
    else
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "You entered an invalid steamID or Nick.")
    end
  else
    nick = string.gsub(nick, '"', "")
    chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], 'You added ', Admin.Colors["VGAM"], nick, Admin.Colors["normal"], ' to ' .. rank .. ".")
    Admin.Player:SetRank(plyFromNick(nick):SteamID(), rank)
  end

end)

/*
  NAME - removeuser
  FUNCTION - Removes a user access
*/
Admin.Commands:createCMD("removeuser", "caller ; user", function(caller, user)
  if !user or user == "" then
    chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "Please enter a steamID or nick.")
    return
  end

  local nick = Admin.Commands:autoComplete( user )
  if nick == nil then
    local sid = string.Split( string.lower(user), ":" )
    if !caller:IsHigherThan( Admin.Player:GetRank(user) ) then
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "You can't target this rank!")
      return
    end
    if string.match(sid[1], "steam_") and isnumber(util.StringToType( sid[2], "Float" )) and isnumber(util.StringToType( sid[2], "Float" )) then
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], 'You removed ', Admin.Colors["VGAM"], user)
      sIDRemove(user)
    else
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "You entered an invalid steamID or Nick.")
    end
  else
    nick = string.gsub(nick, '"', "")
    if !caller:IsHigherThan( Admin.Player:GetRank(plyFromNick(nick):SteamID()) ) then
      chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["error"], "You can't target this rank!")
      return
    end
    chat.Text(caller, Admin.Colors["VGAM"], "[Very Good Admin Mod] ", Admin.Colors["normal"], 'You removed ', Admin.Colors["VGAM"], nick)
    sIDRemove(plyFromNick(nick):SteamID())
  end
end)
