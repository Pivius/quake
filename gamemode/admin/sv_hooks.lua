lastnames = {}
hook.event = {} or hook.event
VGAM = {} or VGAM

function VGAM.hookAdd(event, identifier, func)
  if !event or !identifier or !func then return end
  if hook.event[event]==nil then
		hook.event[event] = {}
	end
    hook.event[event][identifier] = func

end

function VGAM.hookCall(event, ...)
  if !event then return end
  local vararg = ...
  if !vararg then
    vararg = {}
  end
  if hook.event[event] then
    for ident,func in pairs(hook.event[event]) do
      return func(unpack(vararg))
    end

  end
end

function nameCheck()
  for _, ply in pairs(player.GetAll()) do
    UpdateSQL( ply )
    if not lastnames[ply:SteamID()] then lastnames[ply:SteamID()] = ply:Nick() end
    if lastnames[ply:SteamID()] != ply:Nick() then

      hook.Call("nameChange", nil, ply)
      lastnames[ply:SteamID()] = ply:Nick()
    end
  end
end
timer.Create( "nameCheck", 1, 0, nameCheck )
