local meta = FindMetaTable( "Player" )

Admin.Commands = {} or Admin.Commands
Admin.Commands.cmds = {}

function Admin.Commands:LoadCMD()
  local files = file.Find( "quake/gamemode/admin/cmds/*.lua", "LUA" )
  local tbl = {}
  for _,v in pairs( files ) do

    --Admin.Commands.cmds
    if string.match(v, "sv_") then
      include( "quake/gamemode/admin/cmds/" .. v )
    elseif string.match(v, "cl_") then
      if CLIENT then
        include( "quake/gamemode/admin/cmds/" .. v )
      else
        AddCSLuaFile( "quake/gamemode/admin/cmds/" .. v )
      end
    end
  end

end

/*--------------------------------------------------------------------------
 NAME - Notify
 FUNCTION -
--------------------------------------------------------------------------*/
function meta:Notify(ply, cmd, target)
  local note = Notifications:Get( "Admin Success Command", {cmd} )
  if target and target != "" then
    chat.Text(ply, Color(0,75,255), "[Very Good Admin Mod]", Color(255,255,255, 255), note, " on ", target)
  elseif !target then
    chat.Text(ply, Color(0,75,255), "[Very Good Admin Mod]", Color(255,255,255, 255), note )
  end
end


/*--------------------------------------------------------------------------
 NAME - createCMD
 FUNCTION - Creates a command
--------------------------------------------------------------------------*/
function Admin.Commands:createCMD(cmd, ident, callback)

  Admin.Commands.cmds[cmd] = callback
end

/*--------------------------------------------------------------------------
 NAME - autoComplete
 FUNCTION - AutoCompletes name
--------------------------------------------------------------------------*/
function Admin.Commands:autoComplete( varargs )

	local tbl = {}

	for k, v in pairs( player.GetAll() ) do
		local nick = v:Nick()
		if string.find( string.lower( nick ), varargs ) then
			nick = "\"" .. nick .. "\"" -- We put quotes around it incase players have spaces in their names.

			table.insert( tbl, nick )
		end
	end

	return tbl[1]
end

/*--------------------------------------------------------------------------
 NAME - runCMD
 FUNCTION - Calls a hook when using a command
--------------------------------------------------------------------------*/
function meta:runCMD(cmd, ...)
  if !cmd then return end
  local vararg = ...
  local cmd    = string.lower(cmd)
  if !vararg then
    vararg = {}
  end
  /*
  local target = args[2]
  if target == "*" then
    target = "everyone"
  elseif target == "^" then
    target = "yourself"
  end
  */
  local rank = Admin.Ranks[self:GetRank()]
  if Admin.Commands.cmds[cmd] and (UT:TblContain(rank["cmds"], "*") or UT:TblContain(rank["cmds"], cmd))then
    for ident,func in pairs(Admin.Commands.cmds) do
      if ident == cmd then
        return func(self,unpack(vararg))
      end
    end
  end
end


hook.Add("PlayerSay", "CommandCMD", function(ply, txt, team)
	if txt:sub(1, 1) == "!" or txt:sub(1, 1) == "/" then
		txt = txt:sub(2)
		local args = string.Split(txt, " ")
		local cmd = table.remove(args, 1)

		if cmd != "" then

      ply:runCMD(cmd, args)
    else
      chat.Text(ply, Admin.Colors["VGAM"], "[Very Good Admin Mod]", Admin.Colors["error"], " How about you enter a command?" )
    end
      return ""
	end

end)
