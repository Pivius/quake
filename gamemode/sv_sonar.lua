local meta = FindMetaTable( "Player" )
AccessorFunc( meta, "enablesonar", "Sonar", FORCE_BOOL )
AccessorFunc( meta, "checkAFK", "AFK", FORCE_NUMBER )
util.AddNetworkString( "Toggle Sonar" )
local AFKERS = {}
hook.Add("PlayerInitialSpawn", "MakeAFKVar", function(ply)
	ply:SetAFK(CurTime() + 25)
end)

function StandingStill()
	for ent, _ in pairs(AFKERS) do
		if !UT:TblContain(player.GetAll(), ent) then
			AFKERS[ent] = nil
		end
	end
  for _,ply in pairs(player.GetAll()) do
    local vel = ply:GetVelocity()
    if vel != Vector(0,0,0) then
      ply:SetAFK(CurTime() + 25)
    end
    if ply:GetAFK() == nil then
      ply:SetAFK(CurTime() + 25)
    end

    if (CurTime() >= ply:GetAFK()) and ply:Alive() then
      ply:SetSonar(true)
      AFKERS[ply] = true
    else
      ply:SetSonar(false)
      AFKERS[ply] = false
		end
    if ply:GetSonar() == true then

      for k, v in pairs (player.GetAll()) do
        net.Start("Toggle Sonar")
          net.WriteTable(AFKERS)
        net.Send(v)
      end
    else
			table.RemoveByValue( AFKERS, ply )
      for k, v in pairs (player.GetAll()) do
        net.Start("Toggle Sonar")
          net.WriteTable(AFKERS)
        net.Send(v)
      end
    end
  end
end
hook.Add("Think", "Toggle Sonar", StandingStill)
